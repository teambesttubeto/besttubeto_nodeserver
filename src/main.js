import Sequelize from 'sequelize';
import restify from 'restify';
import filesystem from 'fs';
import Q from 'q';
import path from 'path';
import express from 'express';
import DirectionsManager from './managers/DirectionsManager';
import DirectionProcessor from './dataprocessors/DirectionProcessor';
import TFLDirectionProcessor from './dataprocessors/TFLDirectionProcessor';
import advertpositionmappings from './staticdata/advertpositionmappings';
import GoogleMapsAPI from 'googlemaps';
import clc from 'cli-color';
import TFLManager from './managers/TFLManager';

var models = null;

class Main {

  constructor() {
    this.sequelize = null;
    this.server = null;
    this.fileRouter = null;
    this.apiRouter = null;

    this.models = {};
    this.relationships = {};
    this.googleMapsAPI = null;

    this.publicConfig = {
      key: 'AIzaSyDMPxkrsToC-nxQVLczIGPHEnat2nRsf3M',
      stagger_time:       1000, // for elevationPath
      encode_polylines:   false,
      secure:             false, // use https
    };
  }

  initialise(dburl, database, username, password, modelsPath) {
    const deferred = Q.defer();

    this.initialiseSequelize(dburl, database, username, password).then(() => {
      console.log(clc.green("Database initialised"));
      console.log("Initialising models...");
      this.loadModels(modelsPath).then(() =>{
        console.log(clc.green("initialised models"));
        console.log("initialising google maps API...");
        this.googleMapsAPI = new GoogleMapsAPI(this.publicConfig);
        console.log(clc.green("initialised google maps API"));
        deferred.resolve();
      });
    }, (err) => {
      console.log(clc.red('Could not connect to database:'));
      console.log(clc.red(err));
    });

    return deferred.promise;
  }

  initialiseSequelize(dburl, database, username, password){
    console.log("Connecting to database: " + dburl + "/" + database);
    this.sequelize = new Sequelize(database, username, password, {
      host: dburl,
      port: '5432',
      dialect: 'postgres',

      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
    });
    const deferred = Q.defer();

    this.sequelize.authenticate().then((err) => {
      console.log(clc.green("Connection established"));
      deferred.resolve(err);
    })
    .catch((err) => {
      console.log(clc.red("Connection refused: ", err));
      deferred.reject(err);
    });

    return deferred.promise;
  }

  respond(req, res, next) {
    res.send('hello' + req.params.name);
    next();
  }

  getDirections(from, to, pfrom, pto) {
    return DirectionsManager.getDirectionsData(from, to, pfrom, pto);
  }

  getDirectionRequestsToday(from, to) {
    return DirectionsManager.getDirectionRequestsToday(from, to);
  }

  getDirectionRequestsTotal(to) {
    return DirectionsManager.getDirectionRequestsTotal(to);
  }

  getTaxiData(fromId, toId, taxiid) {
    let deferred = Q.defer();
    this.models['station'].findOne({where:{identifier: fromId}}).then((stationRow) => {
      console.log(`find taxi data for direction: ${fromId} - ${toId}`);
      this.models['taxi'].findOne({where:{record:taxiid}}).then((taxiRow) => {
        let taxiArray = [];
        if (taxiRow.cost_a !== null) {
          taxiArray.push({
            cost: taxiRow.cost_a,
            name: stationRow.taxicode1,
            number: stationRow.taxino1,
            info: stationRow.taxiinfo1,
          });
        }
        if (taxiRow.cost_b !== null) {
          taxiArray.push({
            cost: taxiRow.cost_b,
            name: stationRow.taxicode2,
            number: stationRow.taxino2,
            info: stationRow.taxiinfo2,
          });
        }
        if (taxiRow.cost_c !== null) {
          taxiArray.push({
            cost: taxiRow.cost_c,
            name: stationRow.taxicode3,
            number: stationRow.taxino3,
            info: stationRow.taxiinfo3,
          });
        }
        if (taxiRow.cost_d !== null) {
          taxiArray.push({
            cost: taxiRow.cost_d,
            name: stationRow.taxicode4,
            number: stationRow.taxino4,
            info: stationRow.taxiinfo4,
          });
        }
        if (taxiRow.cost_e !== null) {
          taxiArray.push({
            cost: taxiRow.cost_e,
            name: stationRow.taxicode5,
            number: stationRow.taxino5,
            info: stationRow.taxiinfo5,
          });
        }
        deferred.resolve(taxiArray);
      });
    });
    return deferred.promise;
  }

  getStations() {
    console.log("request stations...");
    var deferred = Q.defer();
    this.models['station'].findAll().then((data) => {

      var transformedData = [];
      for (var key in data) {
        var station = data[key].get({plain: true});
        var lng = null;
        var lat = null;
        if (station.stationmapref !== null){
          var textParts = station.stationmapref.split(',');
          lng = textParts[1];
          lat = textParts[0];
        }

        station.stationmapref = { lng, lat };
        transformedData.push(station);
      }

      deferred.resolve(transformedData);
    });

    return deferred.promise;
  }

  getAdvertsUnprocessed(){
    var deferred = Q.defer();

    this.models['advert'].findAll({
      where: {
        $or: [
        {adlatlong: null },
        {placeid: null }
      ]
    }
    }).then((data) => {
      deferred.resolve(data);
    });
    return deferred.promise;
  }

  getAdverts(stationid) {
    var deferred = Q.defer();

    this.models['advert'].findAll({
      where: {
        stationid: stationid,
      }
    }).then((data) => {
      // transform the advert positions into the mappings for ease of use on
      // the frontend.
      //console.log(data);
      var transformedData = new Array();

      for (var key in data) {
        var advert = data[key].get({plain: true});
        //console.log(advert);
        var newAdvertPosition = advertpositionmappings[advert.adposition.toString()];
        //console.log(newAdvertPosition);
        advert.adposition = parseInt(newAdvertPosition);

        var lng = null;
        var lat = null;
        if (advert.adlatlong !== null){
          var textParts = advert.adlatlong.split(',');
          lng = textParts[1];
          lat = textParts[0];
        }

        advert.mapposition = { lng, lat };
        transformedData.push(advert);
      }
      deferred.resolve(transformedData);
    });
    return deferred.promise;
  }

  syncToDatabase() {
    var deferred = Q.defer();
    this.sequelize.sync().then(() => {
      deferred.resolve();
    });
    return deferred.promise;
  }

  processAdverts(data) {
    var funcs = [];
    var deferred = Q.defer();
    console.log("Processing adverts...");
    var chain = data.reduce((previous, item) => {
      return previous.then((previousValue) => {
        return this.processAdvert(item);
      });
    }, Q.resolve()).then(() => {
      deferred.resolve();
    });

    return deferred.promise;
  }

  processAdvert(advertData) {
    var deferred = Q.defer();
    var advertPlain = advertData.get({plain:true});

    this.findMapRef({address:advertPlain.admapref}).then((result) => {
      console.log(clc.green("Successfully found location, updating database record..."));
      console.log(result.geometry.location);
      var latLong = result.geometry.location.lat.toString() + ',' + result.geometry.location.lng.toString();
      var placeid = result.place_id;
      advertData.update({
        adlatlong: latLong,
        placeid: placeid,
      }).then((result) => {
        console.log(result);
        console.log(clc.green("successfully updated record, syncing ORM to database..."));

        advertData.reload().then(() => {
          console.log(clc.green("Synced record to database."));
          deferred.resolve();
        });
      });
    },(err) => {
      console.log(clc.red("Error processing adverts from database: "));
      console.log(clc.red("No admapref for advert '" + advertPlain.adid + "'"));
      deferred.reject(err);
    });

    return deferred;
  }

  populateBTTDirectionTaxiIDAndAlternativeFields() {
    console.log("beginning batch process population of BTT direction table taxi & alternative fields");
    this.models['direction'].findAll({where:{alternative: null},limit:15000}).then((data) => {
      console.log(`${data.length} rows need populating with taxi & alternative data, off we go`);
      this.processDirections(data).then(() => {
        console.log("Finished populating taxi & alternative fields in BTT directions table");
      });
    });
  }

  processDirections(data) {
    var funcs = [];
    var deferred = Q.defer();

    // Let's split the data set into chunks so we can process multiple records
    // at a time.
    var batchSize = 50;
    var batches = this.splitResultSetIntoBatches(data, batchSize);

    console.log("Processing directions...");
    var chain = batches.reduce((previous, item) => {
      return previous.then((previousValue) => {
        console.log(""); // Blank line to seperate logs
        return this.batchProcessDirection(item);
      });
    }, Q.resolve()).then(() => {
      deferred.resolve();
    });

    return deferred.promise;
  }

  batchProcessDirection(set) {
    var deferred = Q.defer();
    var promiseArray = [];
    set.map((row, index) => {
      promiseArray.push(DirectionProcessor.processDirection(row));
    });
    Q.all(promiseArray).then(() => {
      deferred.resolve();
    });
    return deferred;
  }

  processTFLDirections(data) {
    var funcs = [];
    var deferred = Q.defer();

    // Let's split the data set into chunks so we can process multiple records
    // at a time.
    var batchSize = 50;
    var batches = this.splitResultSetIntoBatches(data, batchSize);

    console.log("Processing TFL directions...");
    var chain = batches.reduce((previous, item) => {
      return previous.then((previousValue) => {
        console.log(""); // Blank line to seperate logs
        return this.batchProcessTFLDirection(item);
      });
    }, Q.resolve()).then(() => {
      deferred.resolve();
    });

    return deferred.promise;
  }

  batchProcessTFLDirection(set) {
    var deferred = Q.defer();
    var promiseArray = [];
    set.map((row, index) => {
      promiseArray.push(this.processTFLDirection(row));
    });
    Q.all(promiseArray).then(() => {
      deferred.resolve();
    });
    return deferred.promise;
  }

  splitResultSetIntoBatches(resultSet, batchSize) {
    var batchArray = [];
    var elementCount = 0;
    var batch = [];
    resultSet.map((row, index) => {
      batch.push(row);
      if (elementCount >= batchSize) {
        batchArray.push(batch);
        batch = [];
        elementCount = 0;
      }
      else {
        elementCount++;
      }
    });
    if (elementCount > 0) {
      batchArray.push(batch);
    }

    console.log(`split ${resultSet.length} rows into ${batchArray.length} batches of ${batchSize}.`);
    return batchArray;
  }

  populateTFLRowsWithLineData() {
    var batchSize = 250;

    TFLManager.getTFLDirections(true).then((data) => {
      var batches = this.splitResultSetIntoBatches(data, batchSize);
      TFLDirectionProcessor.populateTFLLineData(batches);
    });
  }

  processTFLDirection(tflDirection) {
    var deferred = Q.defer();
    var plainData = tflDirection.get({plain:true});
    console.log(`processing TFL Direction ${plainData.journeyvector_from} to ${plainData.journeyvector_to}`);
    TFLManager.storeInitialDirectionFromTFL(plainData.journeyvector_from, plainData.journeyvector_to).then(() => {
      deferred.resolve();
    });
    return deferred.promise;
  }

  loadModels(modelsPath) {
    const deferred = Q.defer();

    this.models = {};
    this.relationships = {};

    filesystem.readdirSync(modelsPath).forEach((name) => {
       var object = require(modelsPath + "/" + name).default;
       var options = object.options || {};
       var modelName = name.replace(/\.js$/i, "");

       this.models[modelName] = this.sequelize.define(modelName, object.model, options);
       if("relations" in object){
           this.relationships[modelName] = object.relations;
       }
       console.log(`loaded model ${modelName}`);
     });
     models = this.models;
     console.log(this.models);
     deferred.resolve();
     return deferred.promise;
  }

  findMapRef(geocode) {
    var deferred = Q.defer();
    if (geocode.address === null || geocode.address === undefined) {
      deferred.reject("GMAPI Unable to find location for advert - no admapref on database row.")
    }
    else {
      console.log("GMAPI finding location for " + geocode.address + " ...");
      this.googleMapsAPI.geocode(geocode, ((err, result) => {
        if (err !== undefined && err !== null){
          console.log(err);
          deferred.reject(err);
        }
        else {
          console.log(result);
          deferred.resolve(result.results[0]);
        }
      }));
    }
    return deferred.promise;
  }
}

export { models };
export default new Main;
