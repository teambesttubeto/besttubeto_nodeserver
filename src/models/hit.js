import Sequelize from 'sequelize';

export default {

  model: {
    
    count: {
      type: Sequelize.BIGINT,
    },
    timestamp: {
      type: Sequelize.DATE,
    },
    stationidentifier: {
      type: Sequelize.STRING,
    },
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'hits',
    timestamps: false,
  }
};
