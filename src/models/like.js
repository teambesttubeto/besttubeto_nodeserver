import Sequelize from 'sequelize';

export default {

  model: {
    datetime: {
      type: Sequelize.STRING,
    },
    stationto: {
      type: Sequelize.STRING,
    },
    stationfrom: {
      type: Sequelize.STRING,
    },
    clientip: {
      type: Sequelize.STRING,
    },
    language: {
      type: Sequelize.STRING,
    }
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'likes',
    timestamps: false,
  }
};
