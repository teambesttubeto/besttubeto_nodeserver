import Sequelize from 'sequelize';

export default {

  model: {
    line_id: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    line_colour: {
      type: Sequelize.STRING,
    },
    direction: {
      type: Sequelize.STRING,
    },
    ics_from: {
      type: Sequelize.STRING,
    },
    next_naptan_id: {
      type: Sequelize.STRING,
    },
    station_from: {
      type: Sequelize.STRING,
    },
    naptan_id_from: {
      type: Sequelize.STRING,
    },
    next_station: {
      type: Sequelize.STRING,
    },
    next_ics: {
      type: Sequelize.STRING,
    },
    language: {
      type: Sequelize.STRING,
    },
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'lines',
    timestamps: false,
  }
};
