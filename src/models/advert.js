import Sequelize from 'sequelize';

export default {

  model: {
    adid: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    stationid: {
      type: Sequelize.STRING,
    },
    admapref: {
      type: Sequelize.STRING,
    },
    adname: {
      type: Sequelize.STRING,
    },
    adposition: {
      type: Sequelize.INTEGER,
    },
    adurl: {
      type: Sequelize.STRING,
    },
    adlatlong: {
      type: Sequelize.STRING,
    },
    placeid: {
      type: Sequelize.STRING,
    },
    addata: {
      type: Sequelize.JSON,
    },
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'adverts',
    timestamps: false,
  }
};
