import Sequelize from 'sequelize';

export default {

  model: {

    record: {
      type: Sequelize.BIGINT,
      primaryKey: true,
    },
    miles: {
      type: Sequelize.FLOAT,
    },
    km: {
      type: Sequelize.FLOAT,
    },
    time: {
      type: Sequelize.STRING,
    },
    cost: {
      type: Sequelize.STRING,
    },
    cost_a: {
      type: Sequelize.STRING,
    },
    cost_b: {
      type: Sequelize.STRING,
    },
    cost_c: {
      type: Sequelize.STRING,
    },
    cost_d: {
      type: Sequelize.STRING,
    },
    cost_e: {
      type: Sequelize.STRING,
    },
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'taxi',
    timestamps: false,
  }
};
