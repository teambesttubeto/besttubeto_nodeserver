import Sequelize from 'sequelize';

export default {

  model: {
    btt_route_code: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    btt_journey_name: {
      type: Sequelize.STRING,
    },
    journeyvector_from: {
      type: Sequelize.STRING,
    },
    journeyvector_to: {
      type: Sequelize.STRING,
    },
    journeyvector_uri: {
      type: Sequelize.STRING,
    },
    journeys: {
      type: Sequelize.STRING,
    },
    journeys_legs_arraycount: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_0_path_stoppoints_arraycount: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_0_duration: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_0_instruction_summary: {
      type: Sequelize.STRING,
    },
    line_name_1: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_0_arrivalpoint_icscode: {
      type: Sequelize.STRING,
    },
    change_station_1: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_0_instruction_detailed: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_0_stoppoint0id: {
      type: Sequelize.STRING,
    },
    line_direction_1: {
      type: Sequelize.STRING,
    },
    line_colour_1: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_1_path_stoppoints_arraycount: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_1_duration: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_1_instruction_summary: {
      type: Sequelize.STRING,
    },
    line_name_2: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_1_arrivalpoint_icscode: {
      type: Sequelize.STRING,
    },
    change_station_2: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_1_instruction_detailed: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_1_stoppoint0id: {
      type: Sequelize.STRING,
    },
    line_direction_2: {
      type: Sequelize.STRING,
    },
    line_colour_2: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_2_path_stoppoints_arraycount: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_2_duration: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_2_instruction_summary: {
      type: Sequelize.STRING,
    },
    line_name_3: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_2_arrivalpoint_icscode: {
      type: Sequelize.STRING,
    },
    change_station_3: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_2_instruction_detailed: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_2_stoppoint0id: {
      type: Sequelize.STRING,
    },
    line_direction_3: {
      type: Sequelize.STRING,
    },
    line_colour_3: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_3_path_stoppoints_arraycount: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_3_duration: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_3_instruction_summary: {
      type: Sequelize.STRING,
    },
    line_name_4: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_3_arrivalpoint_icscode: {
      type: Sequelize.STRING,
    },
    change_station_4: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_3_instruction_detailed: {
      type: Sequelize.STRING,
    },
    journeys_0_legs_3_stoppoint0id: {
      type: Sequelize.STRING,
    },
    line_direction_4: {
      type: Sequelize.STRING,
    },
    line_colour_4: {
      type: Sequelize.STRING,
    }
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'b_directions_from_tfl',
    timestamps: false,
  },
};
