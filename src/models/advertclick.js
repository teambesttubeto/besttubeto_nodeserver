import Sequelize from 'sequelize';

export default {

  model: {
    datetime: {
      type: Sequelize.STRING,
    },
    adid: {
      type: Sequelize.STRING,
    },
    adname: {
      type: Sequelize.STRING,
    },
    ipaddress: {
      type: Sequelize.STRING,
    },
    stationto: {
      type: Sequelize.STRING,
    },
    stationfrom: {
      type: Sequelize.STRING,
    },
    language: {
      type: Sequelize.STRING,
    },
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'advertclicks',
    timestamps: false,
  }
};
