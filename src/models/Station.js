import Sequelize from 'sequelize';

export default {

  model: {
    stationid: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    language: {
      type: Sequelize.STRING,
    },
    ics: {
      type: Sequelize.STRING,
    },
    stationname: {
      type: Sequelize.STRING,
    },
    identifier: {
      type: Sequelize.STRING,
    },
    zone: {
      type: Sequelize.STRING,
    },
    step_free: {
      type: Sequelize.STRING,
    },
    deu: {
      type: Sequelize.STRING,
    },
    stationmapref: {
      type: Sequelize.STRING,
    },
    hitstoday: {
      type: Sequelize.BIGINT,
    },
    hitstotal: {
      type: Sequelize.BIGINT,
    },
    taxicode1: {
      type: Sequelize.STRING,
    },
    taxicode2: {
      type: Sequelize.STRING,
    },
    taxicode3: {
      type: Sequelize.STRING,
    },
    taxicode4: {
      type: Sequelize.STRING,
    },
    taxicode5: {
      type: Sequelize.STRING,
    },
    taxino1: {
      type: Sequelize.STRING,
    },
    taxino2: {
      type: Sequelize.STRING,
    },
    taxino3: {
      type: Sequelize.STRING,
    },
    taxino4: {
      type: Sequelize.STRING,
    },
    taxino5: {
      type: Sequelize.STRING,
    },
    taxiinfo1: {
      type: Sequelize.STRING,
    },
    taxiinfo2: {
      type: Sequelize.STRING,
    },
    taxiinfo3: {
      type: Sequelize.STRING,
    },
    taxiinfo4: {
      type: Sequelize.STRING,
    },
    taxiinfo5: {
      type: Sequelize.STRING,
    },
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'stations',
    timestamps: false,
  }
};
