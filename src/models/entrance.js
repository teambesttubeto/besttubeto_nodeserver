import Sequelize from 'sequelize';

export default {

  model: {
    entrance_id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    language: {
      type: Sequelize.STRING,
    },
    line_name: {
      type: Sequelize.STRING,
    },
    direction: {
      type: Sequelize.STRING,
    },
    station_id: {
      type: Sequelize.STRING,
    },
    station_name: {
      type: Sequelize.STRING,
    },
    station_ics: {
      type: Sequelize.STRING,
    },
    in_platform: {
      type: Sequelize.STRING,
    },
    doors_open: {
      type: Sequelize.STRING,
    },
    arrives_from: {
      type: Sequelize.STRING,
    },
    walk: {
      type: Sequelize.INTEGER,
    },
    stairs: {
      type: Sequelize.INTEGER,
    },
    escalators: {
      type: Sequelize.INTEGER,
    },
    lifts: {
      type: Sequelize.INTEGER,
    },
    lifts_available: {
      type: Sequelize.STRING,
    },
    lifts_position: {
      type: Sequelize.STRING,
    },
    time: {
      type: Sequelize.INTEGER,
    },
  },

  relations: {},

  options: {
    schema: 'data',
    tableName: 'entrances',
    timestamps: false,
  }
};
