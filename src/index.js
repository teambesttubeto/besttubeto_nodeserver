import Sequelize from 'sequelize';
import restify from 'restify';
import Main from './main';
import Station from './models/Station';
import express from 'express';
import path from 'path';
import clc from 'cli-color';
import TFLManager from './managers/TFLManager';
import DirectionsManager from './managers/DirectionsManager';
import TaxiManager from './managers/TaxiManager';
import DirectionProcessor from './dataprocessors/DirectionProcessor';
import TFLDirectionProcessor from './dataprocessors/TFLDirectionProcessor';
import EntranceManager from './managers/EntranceManager';
import LikeManager from './managers/LikeManager';
import routeCodes from './staticdata/specifictflroutecodes';
import requestIp from 'request-ip';
import bodyParser from 'body-parser';
import moment from 'moment-timezone';
import Q from 'q';

var htmlDir = "/var/www/html";
var advertDir = "/var/www/advertimages/";
var staticHtmlDir = "/var/www/statichtml";
var config = {};
var database = {
  database: 'btt',
  username: 'rhodri',
  url: 'localhost:5432',
  password: 'bttadmin2017'
};

var initialiseVariables = () => {
  if (config.html !== undefined) {
    htmlDir = config.html;
  }
  if (config.adverts !== undefined) {
    advertDir = config.adverts;
  }
  if (config.database !== undefined) {
    database = config.database;
  }
}

try {
  if (process.env.BTT_CONFIG === undefined) {
    console.log("Environment variable BTT_CONFIG not defined, using default configuration...");
  }
  else {
    console.log("Loading config from " + process.env.BTT_CONFIG);
    config = require(process.env.BTT_CONFIG);
  }
  initialiseVariables();
} catch(e) {
  if (e.code !== 'MODULE_NOT_FOUND') {
    throw e;
  }
  console.log(clc.cyan("No config file found, assuming running in production..."));
}


console.log("Setting HTML directory to " + htmlDir);
console.log("Setting Advert image directory to " + advertDir);

var app = new express();

app.use(requestIp.mw());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use((req, res, next) => {
  console.log("request");
  if (/\/adverts\/[^\/]+/.test(req.url)) {
    let pathSplit = req.url.split('/');
    let imageName = pathSplit[pathSplit.length-1];
    console.log(pathSplit);
    console.log("imagename: " + imageName);
    req.url = advertDir + imageName;
    res.sendFile(req.url);
  }
  else {
    next();
  }
});

app.use((req, res, next) => {
  if (/\/statichtml\/[^\/]+/.test(req.url)) {
    console.log(`request to static html directory: ${req.url}`);
    let htmlFile = req.url.substring(req.url.indexOf('statichtml/')+10);
    console.log("htmlfile: " + htmlFile);
    req.url = staticHtmlDir + htmlFile;
    res.sendFile(req.url);
  }
  else {
    next();
  }
});

console.log("Environment: "+ process.env.NODE_ENV);

app.use(express.static(htmlDir));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get(/^((?!\/api\/).)*$/, (req, res, next) => {
  let routePath = path.resolve(htmlDir, htmlDir, 'index.html');
  console.log('Routing: ' + routePath);
  res.sendFile(routePath);
});

app.get('/api/hello/:name', (req, res, next) => {
  res.send('hello' + req.params.name);
})

app.get('/api/stations', (req, res, next) => {
  Main.getStations().then((stations) => {
    var stationsTransformed = {};
    for (var key in stations){
      var station = stations[key];
      stationsTransformed[station.identifier] = station;
    }
    res.json(stationsTransformed);
  });
});

app.get('/api/stations/directions/:from/:to', (req, res, next) => {
  Main.getDirections(req.params.from, req.params.to).then((directions) => {
    Main.getAdverts(req.params.to).then((adverts) => {
      var data = { directions, adverts };
      res.json(data);
    });
  });
});

app.get('/api/stations/directions/:from/:to/previous/:pfrom/:pto', (req, res, next) => {
  Main.getDirections(req.params.from, req.params.to, req.params.pfrom, req.params.pto).then((directions) => {
    Main.getAdverts(req.params.to).then((adverts) => {
      Main.getTaxiData(req.params.from, req.params.to, directions.taxiid).then((taxiData) => {
        var data = { directions, adverts, taxiData };
        res.json(data);
      });
    });
  });
});

app.get('/api/requests/directions/:from/:to', (req, res, next) => {
  Main.getDirectionRequestsToday(req.params.from, req.params.to).then((requests) => {
    res.json(requests);
  });
});

app.get('/api/adverts/:stationid', (req, res, next) => {
  Main.getAdverts(req.params.stationid).then((adverts) => {
    res.json(adverts);
  });
});

app.get('/api/tfl/:from/:to', (req, res, next) => {
  TFLManager.getData(req.params.from, req.params.to).then((output) => {
    res.json(output);
  })
});
app.get('/api/tfl/raw/:from/:to', (req, res, next) => {
  TFLManager.getTFLData(req.params.from, req.params.to).then((output) => {
    res.json(output);
  })
});
app.get('/api/likes/:from/:to', (req, res, next) => {
  LikeManager.getLikes(req.params.from, req.params.to, req.clientIp).then((output) => {
    res.json(output);
  });
});

app.post('/api/likes/:from/:to', (req, res, next) => {
  console.log(req.body.language);
  LikeManager.like(req.params.from, req.params.to, req.clientIp, req.body.language).then((output) => {
    res.json(output);
  });
});

app.post('/api/advertclicks/:adid', (req, res, next) => {
  console.log(`received ad click request for adid: ${req.params.adid} from ip address: ${req.clientIp}`);
  var datetime = moment.tz("Europe/London").format('YYYY-MM-DD HH:mm:ssZ');
  var language = req.body.language;
  var stationfrom = req.body.stationfrom;
  var stationto = req.body.stationto;
  Main.models['advert'].findOne({where: {adid: req.params.adid}}).then((result) => {
    var plainData = result.get({plain: true});
    Main.models['advertclick'].create(
      {
        adid: req.params.adid,
        adname: plainData.adname,
        ipaddress:req.clientIp,
        datetime,
        stationto,
        stationfrom,
        language,
      });
  });

  res.json("success");
});


Main.initialise(database.url, database.database, database.username, database.password, __dirname + '/models').then(() => {
  console.log("");
  console.log(clc.green("[ SERVER READY ]"));
  console.log("");
  //processBTTDirectionsTaxiAndAlternativeFields();
  stashDailyHitCounts();
  //processSpecificTFLRecords();
  //processBTTDirectionsTaxiAndAlternativeFields();
  //populateTFLTableB();
  //populateBTTDirectionTaxiIDAndAlternativeFields();
  //populateTFLRowsWithLineData();
  //populateBTTDirectionsFromTFLDirections();
  //populateBTTDirectionsDetail();
  //populateTFLTable01();
  cheatHitCounts();


  //processGoogleMapsDistanceRoutine(10, 0);
});


/*****************************************************************/
/*****************************************************************/
// DATABASE ROUTINES:
/*****************************************************************/
/*****************************************************************/
var syncAdverts = () => {
  Main.syncToDatabase().then(() =>{
    Main.getAdvertsUnprocessed().then((data) => {
      console.log("Number of unprocessed adverts: " + data.length);
      unprocessedAdvertCount = data.length;
      if (unprocessedAdvertCount > 0){
        Main.processAdverts(data).then(()=>{
          console.log("Finished processing adverts");
        });
      }
      else {
        console.log("No adverts need processing.");
      }
      console.log(models);
    });
  });
}

var randomRange = function(lower, upper) {
  lower = Math.ceil(lower);
  upper = Math.floor(upper);
  return Math.floor(Math.random() * (upper - lower)) + lower;
}

var stashDailyHitCounts = () => {
  Main.models['station'].findAll().then((resultSet) => {
    resultSet.map((row, index)=>{
      var plainData = row.get({plain:true});
      var stationidentifier = plainData.identifier;
      var timestamp = new Date();
      var count = plainData.hitstoday;

      // We want to fudge the hit counts each day
      // Insert hit row into table
      Main.models['hit'].create({
        stationidentifier,
        count,
        timestamp,
      });
      row.update({
        hitstotal: parseInt(plainData.hitstotal) + parseInt(plainData.hitstoday),
        hitstoday: randomRange(20, 30),
      });
    });
  });
}

var cheatHitCounts = () => {
  Main.models['station'].findAll().then((resultSet) => {
    resultSet.map((row, index)=>{
      let plainData = row.get({plain: true});

      let randomIncrement = Math.floor((Math.random() * 4) + 1);
      row.update({
        hitstoday: parseInt(plainData.hitstoday) + parseInt(randomIncrement),
        histotal: parseInt(plainData.hitstotal) + parseInt(randomIncrement),
      });
    });
  });
}


var unprocessedAdvertCount = 0;
var processingAdverts = false;

var CronJob = require('cron').CronJob;
var job = new CronJob({
  cronTime: '0 * * * * *',
  onTick: function() {
    console.log("run");
    syncAdverts();
  },
  start: false,
  timeZone: 'Europe/London'
});
//job.start();

var job = new CronJob({
  cronTime: '0 0 0 * * *',
  onTick: function() {
    console.log("Stashing daily hit counts");
    stashDailyHitCounts();
  },
  start: false,
  timeZone: 'Europe/London'
});
job.start();
var job = new CronJob({
  cronTime: '*/15 * * * * *',
  onTick: function() {
    console.log("Beginning a run through of API requests to Google...");
    processGoogleMapsDistanceRoutine(100, 0);
  },
  start: false,
  timeZone: 'Europe/London'
});
//job.start();
var job = new CronJob({
  cronTime: '* */20 * * * *',
  onTick: function() {
    console.log("Generating random visitors (cheat)");
    cheatHitCounts();
  },
  start: false,
  timeZone: 'Europe/London'
});
job.start();
var splitResultSetIntoBatches = function(resultSet, batchSize) {
  var batchArray = [];
  var elementCount = 0;
  var batch = [];
  resultSet.map((row, index) => {
    batch.push(row);
    if (elementCount >= batchSize) {
      batchArray.push(batch);
      batch = [];
      elementCount = 0;
    }
    else {
      elementCount++;
    }
  });
  if (elementCount > 0) {
    batchArray.push(batch);
  }

  console.log(`split ${resultSet.length} rows into ${batchArray.length} batches of ${batchSize}.`);
  return batchArray;
}

var batchStoreInitialDirectionFromTFL = function(set) {
  var deferred = Q.defer();
  var promiseArray = [];
  set.map((row, index) => {
    var plainData = row.get({plain:true});
    promiseArray.push(TFLManager.storeInitialDirectionFromTFL(plainData.journeyvector_from, plainData.journeyvector_to));
  });
  Q.all(promiseArray).then(() => {
    deferred.resolve();
  });
  return deferred;
}

var populateTFLTableB = function() {
  Main.models['tfldirectionb'].all().then((resultSet) => {
    console.log(`directions to process ${resultSet.length}`);
    Main.processTFLDirections(resultSet).then(() => {
      console.log(`Finished processing ${resultSet.length} TFL Direction records.`);
    });
  })
}

var populateTFLTable01 = function() {
  console.log(`retrieving all directions to be converted...`);
  TFLManager.getTFLDirections(false).then((resultSet) => {
    console.log(`directions to process ${resultSet.length}`);
    Main.processTFLDirections(resultSet).then(() => {
      console.log(`Finished processing ${resultSet.length} TFL Direction records.`);
    });
  });
}

var populateLineDataForTFLTable = function() {
  console.log(`retrieving all directions to be converted...`);
  TFLManager.getTFLDirectionsForLineData().then((resultSet) => {
    console.log(`directions to process ${resultSet.length}`);
    Main.processTFLDirections(resultSet).then(() => {
      console.log(`Finished processing ${resultSet.length} TFL Direction records.`);
    });
  });
}

var populateTFLRowsWithLineData = function() {
  Main.populateTFLRowsWithLineData();
}

var testTFLResponseSingle = function() {
  TFLManager.storeInitialDirectionFromTFL('1000078', '1000248');
}

var populateBTTDirectionTaxiIDAndAlternativeFields = function() {
  Main.populateBTTDirectionTaxiIDAndAlternativeFields();
}

var performGoogleDirectionsRequestSingle = function() {
  // 1003006
  Main.models['direction'].findAll({where:{icsto:'1000002'}}).then((results) => {
    results.map((direction, row_count) => {
      DirectionProcessor.processDirection(direction);
    });
  });
}

var populateBTTDirectionsFromTFLDirections = function() {
  DirectionsManager.processDirectionBatches();
}

var populateBTTDirectionsDetail = function() {
  DirectionsManager.processDirectionDetailExitBatches();
}

var processSpecificTFLRecords = function() {

  Main.models['tfldirectionb'].findAll({where:{journeyvector_uri: null}}).then((resultSet) => {
    console.log(`directions to process ${resultSet.length}`);
    Main.processTFLDirections(resultSet).then(() => {
      console.log(`Finished processing ${resultSet.length} TFL Direction records.`);
    });
  })
}

var processGoogleMapsDistanceRoutine = function(limit, offset) {
  Main.models['direction'].findAll({where: {alternative: null/*, route: {$notLike: '%heathrow%'}*/, stationto: 'st_johns_wood'}, limit:limit, offset:offset}).then((resultSet) => {
    var promiseArray = [];
    resultSet.map((direction, index) => {
      promiseArray.push(DirectionProcessor.processTaxiId(direction));
    });
    Q.all(promiseArray).then(() => {
      console.log(`finished populating ${resultSet.length} btt direction rows with taxiid and alternative field data.`);
    });
  });
}

var processBTTDirectionsTaxiAndAlternativeFields = function() {
  Main.models['direction'].findAll({where: {alternative: null}, limit:2500}).then((resultSet) => {
    var promiseArray = [];
    resultSet.map((direction, index) => {
      promiseArray.push(DirectionProcessor.processTaxiId(direction));
    });
    Q.all(promiseArray).then(() => {
      console.log(`finished populating ${resultSet.length} btt direction rows with taxiid and alternative field data.`);
    });
  });
}

var server = app.listen(3002);
