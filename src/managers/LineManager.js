import Q from 'q';
import Main from '../main';

class LineManager {

  getLine(icsFrom, naptanId, lineName, lineIndex) {
    const deferred = Q.defer();
/*
    var condition = {where: {ics_from: icsFrom, naptan_id_from: naptanId, line_name:{ $like: `%${lineName}%`}}};

    if (lineIndex === 0) {
      condition = {where: {ics_from: icsFrom, next_naptan_id: naptanId, line_name:{ $like: `%${lineName}%`}}}
    }
    */
    var condition = {where: {ics_from: icsFrom, next_naptan_id: naptanId}}

    Main.models['line'].findOne(condition).then((data) => {
      if (data !== null){
        var plainData = data.get({plain: true});
        deferred.resolve(plainData);
      }
      else {
        deferred.resolve(null);
      }
    });

    return deferred.promise;
  }
}

export default new LineManager();
