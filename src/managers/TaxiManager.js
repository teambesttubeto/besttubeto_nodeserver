import Q from 'q';
import Main from '../main';

class TaxiManager {

  getTaxiTime(kilometers) {
    const deferred = Q.defer();

    Main.models['taxi'].findAll({
      order: [
        ['km', 'ASC'],
      ],
    }).then((data) => {
      if (data !== null){
        var maxTaxiRow = this.findMaxTaxiRow(data, kilometers);
        var plainData = maxTaxiRow.get({plain: true});
        deferred.resolve(plainData);
      }
      else {
        deferred.resolve(null);
      }
    });

    return deferred.promise;
  }

  findMaxTaxiRow(rows, value) {
    var maxRow = rows[0];
    for(var i=0;i<rows.length;i++) {
      var row = rows[i];
      var plainData = row.get({plain: true});
      var nextRow = rows[i+1];
      if (value > plainData.km) {
        maxRow = row;
      }
    }
    return maxRow;
  }
}

export default new TaxiManager();
