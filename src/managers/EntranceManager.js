import Q from 'q';
import Main from '../main';

class EntranceManager {

  getLineKeywordList() {
    return [
      'Bakerloo line',
      'Central line',
      'Circle line',
      'District line',
      'Jubilee line',
      'DLR (Destination: Lewisham, Beckton or Woolwich Arsenal)',
      'DLR (Destination: Bank)',
      'DLR (Destination: Tower Gateway)',
      'DLR (Destination: Beckton or Woolwich Arsenal)',
      'DLR (Destination: Bank or Tower Gateway)',
      'DLR (Destination: Stratford International)',
      'DLR (Destination: Beckton)',
      'DLR (Destination: Bank, Tower Gateway or Stratford)',
      'DLR (Destination: Canning Town, Bank, Tower Gateway or Stratford)',
      'DLR (Destination: Woolwich Arsenal)',
      'DLR (Destination: Canary Wharf or Lewisham)',
      'DLR (Destination: Canary Wharf, Bank, Tower Gateway or Stratford)',
      'DLR (Destination: Lewisham)',
      'DLR (Destination: Stratford)',
      'East London line',
      'Hammersmith & City line',
      'Metropolitan line',
      'Northern line',
      'Piccadilly line',
      'Victoria line',
      'Waterloo & City line'
    ];
  }

  splitWord(keyword) {
    keyword = keyword.replace(",", "");
    keyword = keyword.replace("(", "");
    keyword = keyword.replace(")", "");
    keyword = keyword.replace(":", "");
    keyword = keyword.replace(" line ", " ");
    keyword = keyword.replace(' or ', ' ');
    var splitParts = keyword.split(" ");
    var newArray = [];
    splitParts.map((part, index) => {
      if (part !== '') {
        newArray.push(part);
      }
    });
    return newArray;
  }


  fuzzyMatch(sourceString, phrase) {
    return sourceString === phrase ? 1 : 0;
  }

  fuzzyMatchString(sourceString) {
    var keywords = this.getLineKeywordList();
    var matchingString = '';
    var maxConfidence = 0;

    // There are cases where multiple lines may be used. We only want one though
    // To avoid ambiguity. If the word 'or' is present in the sourcestring,
    // Strip out all other words after and including it.
    var orPosition = sourceString.indexOf(' or ');
    if (orPosition > -1) {
      sourceString = sourceString.substr(0, orPosition);
    }


    keywords.map((keyword, keywordIndex) => {
      var confidence = 0;
      var keywordParts = this.splitWord(keyword);
      console.log(`analyzing keyword: ${keyword}`);
      console.log(`split keywords: ${keywordParts}`);

      keywordParts.map((keywordPart, keywordPartIndex) => {
        var sourceStringParts = this.splitWord(sourceString);
        sourceStringParts.map((sourceStringPart, sourceStringIndex) => {
          var confidenceForKeywordPart = this.fuzzyMatch(keywordPart, sourceStringPart);
          //console.log(`keyword part: ${keywordPart} | word part: ${sourceStringPart} | match: ${confidenceForKeywordPart}`);
          confidence += confidenceForKeywordPart;
        });
      });

      console.log(`confidence in ${keyword} and ${sourceString} matching is ${confidence}`);

      if (confidence > maxConfidence) {
        console.log(`Current match: ${keyword}`);
        maxConfidence = confidence;
        matchingString = keyword;
      }
      console.log("");
    });

    console.log(`for ${sourceString}: found matching string: ${matchingString}`);

    return matchingString;
  }

  getEntrance(line_name, direction, station_ics) {
    const deferred = Q.defer();

    console.log(`finding nearest match for ${line_name}...`);

    //var matchedLineName = this.fuzzyMatchString(line_name);

    //console.log(`retrieving entrance with line_name: ${matchedLineName}, direction: ${direction}, station_ics: ${station_ics}`);

    Main.models['entrance'].findOne({where: {line_name: { $like: `%${line_name}%`}, direction, station_ics}}).then((data) => {
        deferred.resolve(data);
    });

    return deferred.promise;
  }

  decideStandPosition(currentLineName, nextLineName) {
    var standPosition = 'to the MIDDLE of the platform';
    if (currentLineName.indexOf('REAR') > -1) {
      if (nextLineName.indexOf('LEFT') > -1) {
        standPosition = 'to the LEFT of the platform';
      }
      if (nextLineName.indexOf('RIGHT') > -1) {
        standPosition = 'to the RIGHT of the platform';
      }
    }
    if (currentLineName.indexOf('FRONT') > -1) {
      if (nextLineName.indexOf('LEFT') > -1) {
        standPosition = 'to the RIGHT of the platform';
      }
      if (nextLineName.indexOf('RIGHT') > -1) {
        standPosition = 'to the LEFT of the platform';
      }
    }

    return standPosition;
  }
}

export default new EntranceManager();
