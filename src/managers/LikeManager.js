import Q from 'q';
import Main from '../main';
import moment from 'moment-timezone';

class LikeManager {

  getLikes(stationfrom, stationto, clientip) {
    const deferred = Q.defer();

    Main.models['like'].count({where:{stationto: stationto,stationfrom: stationfrom}}).then((count) => {
        deferred.resolve(count);
    });

    return deferred.promise;
  }

  canLike(stationfrom, stationto, clientip) {
    const deferred = Q.defer();

    Main.models['like'].count({where:{stationto: stationto, stationfrom: stationfrom, clientip: clientip}}).then((count) => {
      if (count > 0){
        deferred.resolve(false);
      }
      else {
        deferred.resolve(true);
      }
    });

    return deferred.promise;
  }

  like(stationfrom, stationto, clientip, language) {
    var deferred = Q.defer();
    console.log(language);
    var timestamp = moment.tz("Europe/London").format();
    this.canLike(stationfrom, stationto, clientip).then((canLike) => {
      if (canLike === true) {
        Main.models['like'].create({ stationto: stationto, stationfrom: stationfrom, datetime: timestamp, clientip, language: language}).then(() => {
          this.getLikes(stationfrom, stationto).then((count) => {
            deferred.resolve({
              count: count,
              successful: true,
            });
          });
        });
      }
      else {
        deferred.resolve({
          count: -1,
          successful: false,
        });
      }
    });

    return deferred.promise;
  }
}

export default new LikeManager();
