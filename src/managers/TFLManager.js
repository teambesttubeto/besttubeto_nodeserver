import Q from 'q';
import Main from '../main';
import DirectionsManager from './DirectionsManager';
import https from 'https';
import LineManager from './LineManager';
import ColourManager from './ColourManager';
import TFLDirectionProcessor from '../dataprocessors/TFLDirectionProcessor';


class TFLManager {

  getTFLDirections(includeProcessed=false) {
    var deferred = Q.defer();
    console.log(`Retrieving TFL directions ${includeProcessed === true ? '(including processed rows)' : null}`)
    var condition = includeProcessed === true ? {} : {where:{line_name_1:null}};
    Main.models['tfldirection'].findAll(condition).then((resultSet) => {
      deferred.resolve(resultSet);
    });
    return deferred.promise;
  }

  getTFLDirectionsForLineData(includeProcessed=false) {
    var deferred = Q.defer();
    console.log(`Retrieving TFL directions ${includeProcessed === true ? '(including line-data processed rows)' : null}`)
    var condition = includeProcessed === true ? {} : {where:{line_direction_1:null}};
    Main.models['tfldirection'].findAll(condition).then((resultSet) => {
      deferred.resolve(resultSet);
    });
    return deferred.promise;
  }

  getTFLDirection(icsfrom, icsto) {
    var deferred = Q.defer();
    console.log(`get TFL direction from ${icsfrom} to ${icsto}`);
    Main.models['tfldirection'].find({where:{journeyvector_from: icsfrom, journeyvector_to: icsto}}).then((data) => {
      deferred.resolve(data);
    });
    return deferred.promise;
  }

  getTFLDirectionByRouteCode(btt_route_code) {
    var deferred = Q.defer();
    console.log(`get TFL direction with btt_route_code: ${btt_route_code}`);
    Main.models['tfldirection'].find({where:{btt_route_code}}).then((data) => {
      deferred.resolve(data);
    });
    return deferred.promise;
  }

  getTFLURL() {
    return "https://api.tfl.gov.uk";
  }

  constructJourneyURLPath(from, to, journeypreference='leastinterchange',
                      appid='ec384986', appkey='6dbd804092f814d0fad78d5ba90876b0') {
    return `${this.getTFLURL()}/Journey/JourneyResults/${from}/to/${to}?mode=tube,dlr&time=1200&date=20171005&maxWalkingMinutes=5&journeyPreference=${journeypreference}`;
  }

  constructStopPointURLPath(naptanId, appid='ec384986', appkey='6dbd804092f814d0fad78d5ba90876b0') {
    return `${this.getTFLURL()}/StopPoint/${naptanId}&app_id=${appid}&app_key=${appkey}`;
  }

  getTFLStopPoint(naptanId) {
    var requestURL = this.constructStopPointURLPath(naptanId);
    var deferred = Q.defer();
    https.get(requestURL, (response) => {
      var data = "";
      response.setEncoding('utf8');
      response.on('data', (chunk) =>{
        data += chunk;
      });
      response.on('end', () => {
        deferred.resolve(JSON.parse(data));
      })
    });
    return deferred.promise;
  }

  getTFLData(from, to) {
    var requestURL = this.constructJourneyURLPath(from, to);
    var deferred = Q.defer();
    https.get(requestURL, (response) => {
      var data = "";
      response.setEncoding('utf8');
      response.on('data', (chunk) =>{
        data += chunk;
      });
      response.on('end', () => {
        try {
          var jsonData = JSON.parse(data);
        } catch(e) {
          console.log(`Could not parse JSON Data. ERROR:`);
          console.log(e);
          deferred.reject(e);
        }
        deferred.resolve(jsonData);
      })
    });
    return deferred.promise;
  }

  getTFLCanonicalDirection(fromNaptanId, toNaptanId) {

  }


  getMaxLineCountFromTFLDirection(tflDirection) {
    var lineCount = 1;
    var plainData = tflDirection.get({plain: true});
/*
    while(plainData[`line_colour_${lineCount}`] !== null && plainData[`line_direction_${lineCount}`]) {
      lineCount++;
    }
*/

  return plainData.journeys_legs_arraycount;
  //  return lineCount-1;
  }


  getImportantTFLData(rawTFLData) {
    if (rawTFLData.$type !== undefined) {
      if (rawTFLData.$type.indexOf('DisambiguationResult') > -1) {
        console.error("Cannot use string names for to/from stationid, produces disambiguation results. Use station ICS Codes instead.");
        return null;
      }
    }
    var journey = this.getShortestJourney(rawTFLData.journeys);

    var outputData = {
      general:{
        duration: journey.duration,
        icsfrom: rawTFLData.journeyVector.from,
        icsto: rawTFLData.journeyVector.to
      },
      lines:new Array()
    };
    var lineIndex = 0;

    journey.legs.map((leg, line_count) => {
      var lineData = {};
      lineData.line_index = line_count;
      lineData.departICSCode = leg.departurePoint.icsCode;
      lineData.arriveICSCode = leg.arrivalPoint.icsCode;
      lineData.lineId = leg.routeOptions[0].lineIdentifier.id;
      lineData.lineName = leg.routeOptions[0].lineIdentifier.name;
      lineData.changeStation = leg.instruction.summary.split(' to ')[0];
      lineData.duration = leg.duration;
      lineData.summaryLineName = leg.instruction.summary;
      lineData.detailedLineName = leg.instruction.detailed;
      outputData.lines.push(lineData);
    });
    return outputData;
  }

  getData(from, to){
    var deferred = Q.defer();
    this.getTFLData(from, to).then((rawTFLData) => {
      console.log("Retrieve data from TFL");
      var importantData = this.getImportantTFLData(rawTFLData);
      deferred.resolve(importantData);
    }, (err) => {
      console.log("there was an error :(");
    });
    return deferred.promise;
  }

  convertImportantTFLDataToDirection(stationfromid, stationtoid, importantTFLData) {
    var deferred = Q.defer();
    console.log(`converting TFL directional data to BTT initial directional data for ${stationfromid} to ${stationtoid}`);
    var directionOutputData = {};
    directionOutputData.description = "TEST DESCRIPTION";
    importantTFLData.lines.map((line, index) => {
      directionOutputData[`line_name_${index+1}`] = line.summaryLineName;
      directionOutputData[`change_station_${index+1}`] = line.changeStation;
    });
    this.setLineColours(importantTFLData.lines).then((newLines) => {
      console.log("got colours for lines");
      newLines.map((line, index) => {
        directionOutputData[`line_colour_${index+1}`] = line.lineColour;
      });
      deferred.resolve(directionOutputData);
    })
    return deferred.promise;
  }

  convertImportantTFLDataToDirection(stationfromid, stationtoid, importantTFLData) {
    var deferred = Q.defer();
    console.log(`converting TFL directional data to BTT directional data for ${stationfromid} to ${stationtoid}`);
    var directionOutputData = {};
    directionOutputData.description = "TEST DESCRIPTION";
    importantTFLData.lines.map((line, index) => {
      directionOutputData[`line_name_${index+1}`] = line.summaryLineName;
      directionOutputData[`change_station_${index+1}`] = line.changeStation;
    });
    this.setLineColours(importantTFLData.lines).then((newLines) => {
      console.log("got colours for lines");
      newLines.map((line, index) => {
        directionOutputData[`line_colour_${index+1}`] = line.lineColour;
      });
      deferred.resolve(directionOutputData);
    })
    return deferred.promise;
  }

  setLineColours(lines) {
    console.log("set line colours");
    var deferred = Q.defer();
    var promises = [];

    lines.map((line, count) => {
      var colourDeferred = Q.defer();
      LineManager.getLine(line.lineId).then((bttLine)=>{
        ColourManager.getColour(bttLine.line_colour).then((colour) => {
          colourDeferred.resolve(colour);
        });
      });
      promises.push(colourDeferred.promise);
    });
    Q.all(promises).then((newItems) => {
      console.log(newItems);
      newItems.map((colour, index) => {
        lines[index].lineColour = colour.colour;
      });
      deferred.resolve(lines);
    });
    return deferred.promise;
  }
/*
  populateDatabase(directionOutputData) {
    return this.upsert(directionOutputData, directionOutputData.stationfrom, directionOutputData.stationto);
  }

  upsert(values, from, to) {
    return Main.models['direction'].findOne({
      stationfrom: from,
      stationto: to
    }).then((result) => {
      if (result) {
        // already exists, update and return
        return result.update(values);
      }
      else {
        return Main.models['direction'].create(values);
      }
    });
  }
*/

  updateDatabaseRecord(from, to, directionOutputData) {
    var deferred = Q.defer();
    var direction = Main.models['direction'].findOne({
      where: {
        icsfrom: from,
        icsto: to
      }
    }).then((result)=>{
      if (result) {
        result.update(directionOutputData).then((updateResult) => {
          deferred.resolve(result);
        });
      }
      else {
        deferred.reject(result);
      }
    });
    return deferred.promise;
  }

  updateTFLDirectionDatabaseRecord(from, to, convertedDirectionData) {
    var deferred = Q.defer();
    var tflDirection = Main.models['tfldirectionb'].findOne({
      where: {
        btt_route_code: convertedDirectionData.btt_route_code
      }
    }).then((result)=>{
      if (result) {
        result.update(convertedDirectionData).then((updateResult) => {
          deferred.resolve(result);
        });
      }
      else {
        deferred.reject(result);
      }
    });
    return deferred.promise;
  }

  storeDirectionFromTFL(from, to) {
    console.log(`Beginning storing of directional data from TFL to BTT database for ${from} to ${to}`);
    this.getData(from, to).then((importantTFLData) => {
      console.log(`Retrieved data from TFL`);
      this.convertImportantTFLDataToDirection(from, to, importantTFLData).then((convertedDirectionData) => {
        this.updateDatabaseRecord(from, to, convertedDirectionData).then((updateResult) => {
          console.log(`Updated database with new direction data from TFL for ${from} to ${to}`);
        });
      });
    });
  }
  storeInitialDirectionFromTFL(from, to) {
    var deferred = Q.defer();
    console.log(`Beginning storing of initial directional data from TFL to BTT database for ${from} to ${to}`);
    this.getTFLData(from, to).then((rawTFLData) => {
      console.log(`Retrieved raw data from TFL`);
      TFLDirectionProcessor.convertTFLDataToTFLDirection(rawTFLData).then((convertedDirectionDataArray) => {
        console.log("Converted TFL Data to TFL Directions");
        console.log(convertedDirectionDataArray);
        convertedDirectionDataArray.map((convertedDirectionData, index) => {
          this.updateTFLDirectionDatabaseRecord(rawTFLData.journeyVector.from, rawTFLData.journeyVector.to, convertedDirectionData).then((result) => {
            console.log(`Updated TFL direction table with new directions data from TFL for ${rawTFLData.journeyVector.from} to ${rawTFLData.journeyVector.to} (${convertedDirectionData})`);
            deferred.resolve(result);
          });
        });
      });
    });
    return deferred.promise;
  }
}


export default new TFLManager();
