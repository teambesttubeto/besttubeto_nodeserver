import Q from 'q';
import Main from '../main';

class StationManager {

  getStationByICSCode(icsCode) {
    const deferred = Q.defer();

    Main.models['station'].find({where: {ics: icsCode}}).then((data) => {
      if (data !== null){
        var plainData = data.get({plain: true});
        deferred.resolve(plainData);
      }
      else {
        deferred.resolve(null);
      }
    });

    return deferred.promise;
  }

  getStations(limit, offset) {
    const deferred = Q.defer();

    Main.models['station'].findAll({limit, offset}).then((data) => {
      console.log(data.length);
      deferred.resolve(data);
    });

    return deferred.promise;
  }

  getStationCount() {
    const deferred = Q.defer();
    Main.models['station'].count().then((count) => {
      deferred.resolve(count);
    });
    return deferred.promise;
  }
}

export default new StationManager();
