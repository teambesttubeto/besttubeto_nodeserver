import STATION_DATA_1 from '../staticdata/directions_victoria';
import STATION_DATA_2 from '../staticdata/directions_wembleypark';
import STATION_DATA_3 from '../staticdata/directions_westminster';
import Q from 'q';
import Main from '../main';
import ColourManager from './ColourManager';
import distance from 'google-distance';
import StationManager from './StationManager';
import TaxiManager from './TaxiManager';
import DirectionProcessor from '../dataprocessors/DirectionProcessor';


distance.apiKey = 'AIzaSyDMPxkrsToC-nxQVLczIGPHEnat2nRsf3M';

class DirectionsManager {

  processDirectionBatches() {
    console.log("beginning processing of TFL directions to BTT Directions");

    const batchSize = 25;
    var deferred = Q.defer();

    this.processDirectionBatch(300, batchSize).then(() => {
      console.log("finished processing directions");
      deferred.resolve();
    });

    /*

    .then(this.processDirectionBatch(50, batchSize)
    .then(this.processDirectionBatch(100, batchSize)
    .then(this.processDirectionBatch(150, batchSize)
    .then(this.processDirectionBatch(200, batchSize)
    .then(this.processDirectionBatch(250, batchSize)
    .then(this.processDirectionBatch(350, batchSize)
    .then(()=>{ deferred.resolve(); })
    ))))));
    */


    return deferred.promise;
  }

  processDirectionDetailBatches() {

    console.log("beginning processing of TFL directions detail to BTT Directions detail");

    const batchSize = 15000;
    var deferred = Q.defer();
    var promises = [];

    promises.push(this.processDirectionDetailBatch(0, batchSize-1).then(() => {
      this.processDirectionDetailBatch(15000, batchSize-1).then(() => {
        this.processDirectionDetailBatch(30000, batchSize-1).then(() => {
          this.processDirectionDetailBatch(45000, batchSize-1).then(() => {
            this.processDirectionDetailBatch(60000, batchSize-1).then(() => {

            });
          });
        });
      });
    }));



    promises.push(this.processDirectionDetailBatch(75000, batchSize-1).then(() => {
      this.processDirectionDetailBatch(90000, batchSize-1).then(() => {
      });
    }));
    Q.all(promises).then(() => {
      deferred.resolve();
    })
    return deferred.promise;
  }

  processDirectionDetailExitBatches() {

    console.log("beginning processing of TFL directions detail to BTT Directions detail (for exits)");

    const batchSize = 15000;
    var deferred = Q.defer();
    var promises = [];

    promises.push(this.processDirectionDetailExitBatch(0, batchSize-1).then(() => {
      this.processDirectionDetailExitBatchprocessDirectionDetailExitBatch(15000, batchSize-1).then(() => {
        this.processDirectionDetailExitBatch(30000, batchSize-1).then(() => {
        });
      });
    }));

    promises.push(this.processDirectionDetailExitBatchprocessDirectionDetailExitBatch(45000, batchSize-1).then(() => {
      this.processDirectionDetailExitBatch(60000, batchSize-1).then(() => {

      });
    }));

    promises.push(this.processDirectionDetailExitBatchprocessDirectionDetailExitBatch(75000, batchSize-1).then(() => {
      this.processDirectionDetailExitBatch(90000, batchSize-1).then(() => {
      });
    }));
    Q.all(promises).then(() => {
      deferred.resolve();
    })
    return deferred.promise;
  }


  processDirectionDetailBatch(offset, batchSize) {
    var deferred = Q.defer();

    this.getDirections(batchSize, offset).then((directions) => {
      //var batches = Main.splitResultSetIntoBatches(stations, batchSize);
      console.log(`directions batch length: ${directions.length}`);
      console.log("Processing directions detail...");
      var chain = directions.reduce((previous, item) => {
        return previous.then((previousValue) => {
          console.log(""); // Blank line to seperate logs
          return DirectionProcessor.populateDirectionDetail(item);
        });
      }, Q.resolve()).then(() => {
        deferred.resolve();
      });
    });
    return deferred.promise;
  }

  processDirectionDetailExitBatch(offset, batchSize) {
    var deferred = Q.defer();

    this.getDirections(batchSize, offset).then((directions) => {
      //var batches = Main.splitResultSetIntoBatches(stations, batchSize);
      console.log(`directions batch length: ${directions.length}`);
      console.log("Processing directions detail...");
      var chain = directions.reduce((previous, item) => {
        return previous.then((previousValue) => {
          console.log(""); // Blank line to seperate logs
          return DirectionProcessor.populateDirectionDetailExit(item);
        });
      }, Q.resolve()).then(() => {
        deferred.resolve();
      });
    });
    return deferred.promise;
  }

  getDirections(batchSize, offset) {
    var deferred = Q.defer();

    Main.models['direction'].findAll({limit:batchSize, offset}).then((directions) => {
      deferred.resolve(directions);
    });

    return deferred.promise;
  }

  processDirectionBatch(offset, batchSize) {
    var deferred = Q.defer();

    StationManager.getStations(batchSize, offset).then((stations) => {
      //var batches = Main.splitResultSetIntoBatches(stations, batchSize);
      console.log(stations);
      console.log("Processing directions...");
      var chain = stations.reduce((previous, item) => {
        return previous.then((previousValue) => {
          console.log(""); // Blank line to seperate logs
          var plainData = item.get({plain: true});
          return this.processDirectionsTo(plainData.ics);
        });
      }, Q.resolve()).then(() => {
        deferred.resolve();
      });
    });
    return deferred.promise;
  }
/*
  console.log("Processing TFL directions...");
  var chain = batches.reduce((previous, item) => {
    return previous.then((previousValue) => {
      console.log(""); // Blank line to seperate logs
      return this.batchProcessTFLDirection(item);
    });
  }, Q.resolve()).then(() => {
    deferred.resolve();
  });
  */

  processDirectionsToBatch(batch) {
    var deferred = Q.defer();
    var promises = [];

    for (var i = 0;i<Math.ceil(batch.length); i++) {
      var plainData = batch[i].get({plain: true});
      var promise = this.processDirectionsTo(plainData.ics);
      promises.push(promise);
    }
    Q.all(promises).then(() => {
      deferred.resolve();
    });


    return deferred.promise;
  }

  processDirectionsTo(icsTo) {
    console.log(`processing directions to ${icsTo}`);
    var deferred = Q.defer();
    this.getDirectionsTo(icsTo, true).then((data) => {
      Main.processDirections(data).then((processedData) => {
        console.log(`finished processing ${processedData.length} direction rows`);
        deferred.resolve();
      });
    });
    return deferred;
  }

  getUnprocessedDirectionsAsBatches() {
    var deferred = Q.defer();
    var directionBatchArray = [];
    var promises = [];
    var batchSize = 10000;

    this.getDirectionsUnprocessedCount().then((count) => {
      console.log(`total amount of unprocessed directions: ${count}`);
      for (var i = 0;i<Math.ceil(count/batchSize); i++) {
        var promise = this.getUnprocessedDirections(batchSize, batchSize * i).then((batch, i) => {
          console.log(`retrieved batch of directions (${batch.length})`);
          directionBatchArray.push(batch);
        });
        promises.push(promise);
      }
      Q.all(promises).then(() => {
        deferred.resolve(directionBatchArray);
      });
    });

    return deferred.promise;
  }

  getUnprocessedDirections(limit, offset) {
    const deferred = Q.defer();
    Main.models['direction'].findAll({where: {line_name_1: null}, limit, offset}).then((data) => {
      console.log(`Got unprocessed directions limit:${limit} offset:${offset} length:${data.length}`);
      deferred.resolve(data);
    });
    return deferred.promise;
  }

  getDirectionsUnprocessedCount() {
    const deferred = Q.defer();
    Main.models['direction'].count({where: {line_name_1: null}}).then((count) => {
      deferred.resolve(count);
    });
    return deferred.promise;
  }

  getDirectionsTo(icsTo, includeProcessed=false) {
    const deferred = Q.defer();
    var condition = includeProcessed === true ? {where: {icsto: icsTo}} : {where: {icsto: icsTo, line_name_1: null}};
    Main.models['direction'].findAll(condition).then((data) => {
      deferred.resolve(data);
    });
    return deferred.promise;
  }

  getDirectionsDataTest(stationFrom, stationTo) {
    // For now just return the default data for eastputney to victoria:
    let stationData = STATION_DATA_1;

    if (stationFrom === 'east_putney' && stationTo === 'victoria') {
      stationData = STATION_DATA_2;
    }

    if (stationFrom === 'east_putney' && stationTo === 'westminster') {
      stationData = STATION_DATA_3;
    }

    return stationData;
  }

  getRawDirectionsByICS(from, to) {
    const deferred = Q.defer();
    console.log("request directions from " + from + " to " + to + "... ");

    Main.models['direction'].findOne({where: {icsto: to, icsfrom: from}}).then((data) => {
      //console.log(data);
      deferred.resolve(data);
    });

    return deferred.promise;
  }

  getDirectionRequestsTotal(to) {
    const deferred = Q.defer();

    Main.models['stations'].findOne(
      {
        where: {
          identifier: to,
        },
      }).then((station) => {
      var plainData = station.get({plain: true});
      deferred.resolve(plainData.hitstotal);
    });
    return deferred.promise;
  }

  constructGeocodeFromStationName(direction, stationType) {

    var firstPart = "";
    var secondPart = "";
    var directionData = direction.get({plain:true});


    console.log("a");
    if (stationType === 'from') {
      if (directionData.stationfrom.indexOf('heathrow_t12&3') > -1) {
        return 'Heathrow Terminals 1-2-3, Longford';
      }
      firstPart = directionData.stationfrom.replace("_", " ");
      secondPart = directionData.stationfrom_title;
    }
    else {
      if (directionData.stationto.indexOf('heathrow_t12&3') > -1) {
        return 'Heathrow Terminals 1-2-3, Longford';
      }
      firstPart = directionData.stationto.replace("_", " ");
      secondPart = directionData.stationto_title;
    }
    console.log("d");
    return `${firstPart} ${secondPart}`;
  }

  getGoogleDistanceData(fromStationICS, toStationICS, useLatLong = true) {

    var deferred = Q.defer();

    console.log(`Getting google distance from ${fromStationICS} to ${toStationICS}`);

    var fromLatLong = "";
    var toLatLong = "";

    var stationFromPromise = Q.defer();
    var stationToPromise = Q.defer();
    var stationFromName = "";
    var stationToName = "";

    if (useLatLong === true) {
      // We're going to use the latitude and longitude of the to and from
      // station in the google distance matrix API request.
      StationManager.getStationByICSCode(fromStationICS).then((station) => {
        fromLatLong = station.stationmapref;
        stationFromName = station.stationname;
        stationFromPromise.resolve(station);
      });
      StationManager.getStationByICSCode(toStationICS).then((station) => {
        toLatLong = station.stationmapref;
        stationToName = station.stationname;
        stationToPromise.resolve(station);
      });
    }
    else {
      // We're going to pass google's distance matrix API the names of the
      // to and from stations, in the hope that it will use it's geocoding
      // API to successfully locate both stations.
      console.log(`useLatLong is false, we're going to use the geocode stuff...`);
      Main.models['direction'].findOne({where:{icsfrom:fromStationICS, icsto:toStationICS}}).then((direction) => {
        console.log("Got the direction");
        stationFromName = this.constructGeocodeFromStationName(direction, 'from');
        stationToName = this.constructGeocodeFromStationName(direction, 'to');

        console.log(`Perform google distance matrix API request from ${stationFromName} to ${stationToName}`);

        distance.get({
          origin: stationFromName,
          destination: stationToName,
          mode: 'transit',
          transit_mode: ['tram', 'subway']
        }, (err, data) => {
          if (err) {
            console.log(`Error finding ${stationFromName} to ${stationToName}`);
            console.log(err);
            deferred.reject(err);
          }
          else {
            // get taxi time row
            console.log(data);
            console.log(`Google says the distance between ${stationFromName} and ${stationToName} is ${data.distance}km`);
            TaxiManager.getTaxiTime(data.distanceValue / 1000).then((taxiData) => {
              deferred.resolve(taxiData);
            });
          }
        });
      });
    }
    Q.all(stationToPromise.promise, stationFromPromise.promise).then(()=>{
      console.log(`Perform google distance matrix API request from ${stationFromName}(${fromLatLong}) to ${stationToName}(${toLatLong})`);

      distance.get({
        origin: useLatLong === true ? fromLatLong : stationFromName,
        destination: toLatLong === true ? toLatLong : stationToName,
        mode: 'walking',
      }, (err, data) => {
        if (err) {
          console.log(err);
          deferred.reject(err);
        }
        else {
          // get taxi time row
          console.log(`Google says the distance between ${stationFromName}(${fromLatLong}) and ${stationToName}(${toLatLong}) is ${data.distanceValue}km`);
          TaxiManager.getTaxiTime(data.distanceValue / 1000).then((taxiData) => {
            deferred.resolve(taxiData);
          });
        }
      });
    });
    return deferred.promise;
  }

  getDirectionRequestsToday(from, to) {
    const deferred = Q.defer();

    Main.models['stations'].findOne(
      {
        where: {
          identifier: to,
        },
      }).then((station) => {
      var plainData = station.get({plain: true});
      deferred.resolve(plainData.hitstoday);
    });

    return deferred.promise;
  }

  getDirectionsData(from, to, pfrom, pto) {
    const deferred = Q.defer();
    console.log("request directions from " + from + " to " + to + "... ");

    Main.models['direction'].findAll({where: {stationto: to, stationfrom: from}}).then((data) => {
      //console.log(data);
      if (data.length > 0){
        var plainData = data[0].get({plain: true});
        //deferred.resolve(data);
        this.buildDirectionsData(plainData).then((items) => {
          Main.models['station'].findOne({where:{identifier: to}}).then((row) => {
            console.log("STATION ROW");
            console.log(row);
            if (to !== pto){
              row.increment(['hitstoday', 'hitstotal']).then(() => {
                row.reload().then((refreshedRow) => {
                  var stationPlainData = refreshedRow.get({plain: true});
                  const hitstotal = stationPlainData.hitstotal;
                  const hitstoday = stationPlainData.hitstoday;
                  items.hits = {hitstoday, hitstotal};
                  items.route_code = plainData.route_code;
                  items.taxiid = plainData.taxiid;
                  deferred.resolve(items);
                });
              });
            }
            else {
              var stationPlainData = row.get({plain: true});
              const hitstoday = stationPlainData.hitstoday;
              const hitstotal = stationPlainData.hitstotal;
              items.hits = {hitstoday, hitstotal};
              items.route_code = plainData.route_code;
              items.taxiid = plainData.taxiid;
              deferred.resolve(items);
            }
          });
        });
      }
      else {
        deferred.resolve(null);
      }
    });

    // TEST CODE
    //deferred.resolve(this.getDirectionsDataTest(from, to));
    return deferred.promise;
  }

  buildDirectionsData(data) {
    if (data === undefined || data === null) return {};
    var deferred = Q.defer();

    var rawData = data;
    //var items = this.buildDirections(rawData);
    this.buildDirections(rawData).then((items) => {
      console.log("DIRECTION RAW DATA:");
      console.log(rawData);
      var detailSummary = {finalStep: rawData.notes_from_station, totalTravelTime: rawData.total_travel_time};
      var summarySummary = {travelOverview: rawData.total_travel_details, totalTravelTime: rawData.total_travel_time, journeyOverview: rawData.alternative};
      var mainObj = {general: {
        description: rawData.description,
        note_from_station: rawData.note_from_station,
      },directions: items, detailSummary, summarySummary};
      deferred.resolve(mainObj);
    });

    return deferred.promise;
  }

  checkForLineData(data, line_count) {
    var line_data = data[`line_colour_${line_count}`];

    var result =  (line_data !== undefined && line_data !== null);
    return result;
  }

  getLineVisible(line_data) {
    return line_data.line_colour !== null;
  }

  buildDirections(data) {
    var items = [];

    items.push(this.buildDirection(data, '1'));
    items.push(this.buildDirection(data, '2'));
    items.push(this.buildDirection(data, '3'));
    items.push(this.buildDirection(data, '4'));

    this.setSequentialStationLinks(items);
    var lastDirectionIndex = this.setVisibilityOfStations(items);

    if (data.exit_name_5 !== null) {
      // If there's data in the final line then we need to link that up with
      // the last entry in the items array
      var item = items[3];
      item.overview.end_station_link = data.exit_name_5;
      item.overview.line_direction_link = data.note_from_station_5;
      item.overview.change_station_line = data.exit_name_5;
      item.overview.change_station_details_link = data.exit_details_5;
    }

    items = this.remapVisibleDirectionItems(items);

    items = this.remapDirectionTimes(items, data, lastDirectionIndex);

    var deferred = Q.defer();

    this.setColourOfStations(items).then((newItems) => {
      deferred.resolve(newItems);
    });

    return deferred.promise;
  }

  remapDirectionTimes(items, data, lastDirectionIndex) {
    var newItems = [];
    items.map((item, line_count) => {
      item.overview = this.amendForWeirdLogic(item.overview, data, line_count, lastDirectionIndex);
      newItems.push(item);
    });
    return newItems;
  }

  setSequentialStationLinks(items) {
    items.map((item, index) => {
      // Perform this for all items in the array except the last one
      item.overview.change_station_line = null;
      item.overview.line_direction_link = null;
      item.overview.end_station_link = null;
      item.overview.change_station_details_link = null;

      if (index < items.length-1) {
        item.overview.end_station_link = items[index+1].overview.end_station;
        item.overview.line_direction_link = items[index+1].overview.line_direction;
        item.overview.change_station_line = items[index+1].overview.line_name;
        item.overview.change_station_details_link = items[index+1].overview.change_details;
      }
    });
  }

  remapVisibleDirectionItems(items){
    var newItems = [];
    items.map((item, index) => {
      if (item.direction_visible === true) {
        newItems.push(item);
        item.isLastLine = false;
      }
    });
    newItems[newItems.length-1].isLastLine = true;
    return newItems;
  }

  setVisibilityOfStations(items){
    var lastDirectionIndex = 0;
    items.map((item, index) => {
      if (this.getLineVisible(item.overview)){
        item.direction_visible = true;
        lastDirectionIndex ++;
      } else {
        item.direction_visible = false;
      }
    });
    return lastDirectionIndex;
  }

  setColourOfStations(items) {
    var deferred = Q.defer();
    var promises = [];
    items.map((item) => {
      promises.push(ColourManager.getColour(item.overview.line_colour));
    });
    Q.all(promises).then((newItems) => {
      newItems.map((item, index) => {
        items[index].overview.line_colour = item.colourcode;
        items[index].overview.line_colour_name = item.colour_name;
        items[index].overview.text_colour = item.text_colour;
      });
      deferred.resolve(items);
    });
    return deferred.promise;
  }

  buildDirection(data, line_count){
    var overview = this.buildDirectionOverview(data, line_count);
    //var detail = this.buildDirectionDetail(data, line_count);
    overview.line_count = parseInt(line_count);
    return {overview};
  }

  buildDirectionOverview(data, line_count) {
    var overview = {};
    overview.line_name = data[`line_name_${line_count}`];
    overview.line_colour = data[`line_colour_${line_count}`];
    overview.line_direction = data[`line_direction_${line_count}`];
    overview.end_station = data[`end_station_${line_count}`];
    overview.line_platform = data[`line_platform_${line_count}`];
    overview.entrance_details = data[`entrance_details_${line_count}`];
    overview.entrance_time = data[`entrance_time_${line_count}`];
    overview.stand_position = data[`stand_position_${line_count}`];
    overview.arrives_from = data[`arrives_from_${line_count}`];
    overview.board_position = data[`board_position_${line_count}`];
    overview.change_station = data[`change_station_${line_count}`];
    overview.change_details = data[`change_details_${line_count}`];
    overview.station_count = data[`station_count_${line_count}`];
    overview.station_count_text = data[`station_count_text_${line_count}`];
    overview.journey_time = data[`journey_time_${line_count}`];
    overview.journey_exit = data[`journey_exit_${line_count}`];
    if (parseInt(line_count)<4) {
      overview.line_platform_link = data[`line_platform_${(parseInt(line_count)+1).toString()}`];
    }
    else {
      overview.line_platform_link = null;
    }
    return overview;
  }

  amendForWeirdLogic(overview, data, line_count, lastDirectionIndex) {

    if (parseInt(line_count) > 0) {
      overview.entrance_time = data[`change_time_${(parseInt(line_count)+1).toString()}`];
    }

    if (parseInt(line_count+1) === lastDirectionIndex) {
      // This is the last line
      if (lastDirectionIndex !== 4) {
        var nextItemIndex = (parseInt(lastDirectionIndex) + 1).toString();

        overview.exit_time = data[`change_time_${nextItemIndex}`];
      }
      else {
        overview.exit_time = data[`exit_time_5`];
      }
    }
    return overview;
  }

  getDirectionsFromTo(from, to) {
    return this.getDirectionsData(from, to);
  }
}

export default new DirectionsManager();
