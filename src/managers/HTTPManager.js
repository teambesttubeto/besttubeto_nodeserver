import Q from 'q';
import https from 'https';
import http from 'http';

class HTTPManager {
  getData(url, ssl=true) {
    var deferred = Q.defer();
    const protocol = ssl ? https : http;
    protocol.get(url, (response) => {
      var data = "";
      response.setEncoding('utf8');
      response.on('data', (chunk) =>{
        data += chunk;
      });
      response.on('end', () => {
        deferred.resolve(JSON.parse(data));
      })
    }).on('error', (e) => {
      console.log(`${ssl ? 'HTTPS' : 'HTTP'} GET ERROR`);
      console.log(e);
      deferred.reject(e);
    });
    return deferred.promise;
  }
}

export default new HTTPManager();
