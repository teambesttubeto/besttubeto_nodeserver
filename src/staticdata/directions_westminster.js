export default {
  directions: [
    {
      overview: {
        line_name: 'DLR',
        line_colour: '#00FF00',
        line_direction: 'Southbound',
        end_station: 'Stratford',
        line_platform: 'Platform 2',
        entrance_details: '40m walk + 25 stairs (LIFT available)',
        entrance_time: '1 minute',
        stand_position: 'LEFT',
        arrives_from: 'RIGHT',
        board_position: 'at the FRONT (LEFT)',
        change_station: 'West Ham',
        station_count: '1st',
        journey_time: '1 minute',
        journey_exit: 'FRONT RIGHT',
      },
    },
    {
      overview: {
        line_name: 'District Line',
        line_colour: '#0000FF',
        line_direction: 'Westbound',
        end_station: 'Ealing Broadway via Tower Hill',
        line_platform: 'Platform 1',
        entrance_details: '160m walk + 55 stairs + 1 escalator (LIFTS available)',
        entrance_time: '3 minutes',
        stand_position: 'LEFT',
        arrives_from: 'LEFT',
        board_position: 'at the FRONT (RIGHT)',
        change_station: '',
        station_count: '',
        journey_time: '',
        journey_exit: '',
      },
    },
  ],
  'detailSummary': {
    'finalStep':'Putney Centre is LEFT out of the Tube Station - 5 minutes walk. ',
    'totalTravelTime':'Total travelling time 55-60 minutes.',
  },

  'summarySummary': {
    'travelOverview':'155m walk, 80 stairs, 1 escalator.',
    'totalTravelTime':'Total travelling time 55-60 minutes',
  },
};
