import Q from 'q';
import LineManager from '../managers/LineManager';
import ColourManager from '../managers/ColourManager';
import StationManager from '../managers/StationManager';
import DirectionsManager from '../managers/DirectionsManager';
import TFLManager from '../managers/TFLManager';
import ordinal from 'ordinal';
import EntranceManager from '../managers/EntranceManager';


class DirectionProcessor {

  processDirection(direction) {
    var deferred = Q.defer();
    var plainData = direction.get({plain:true});
    var promises = [];
    console.log(`processing direction from ${plainData.icsfrom} to ${plainData.icsto}...`);
    // Set the alternative field if necessary


    if (plainData.alternative === null) {
      var taxiPromise = Q.defer();
      this.processTaxiId(direction).then((result) => {
        taxiPromise.resolve(result);
      });
      promises.push(taxiPromise);
    }
/*
    var tflPromise = Q.defer();
    TFLManager.getTFLDirection(plainData.icsfrom, plainData.icsto).then((tflDirection) => {
      console.log("got");
      var directionData = this.convertTFLDirectionToBTTDirectionData(tflDirection);
      direction.update(directionData).then(() => {
        console.log(`updated direction from ${plainData.icsfrom} to ${plainData.icsto}`);
        tflPromise.resolve();
      });
    });

    promises.push(tflPromise);
*/

    Q.all(promises).then(() => {
      deferred.resolve();
    })
    return deferred.promise;
  }

  convertTFLDirectionToBTTDirectionData(tflDirection) {
    var directionData = {};

    var plainData = tflDirection.get({plain:true});

    var maxLineCount = parseInt(plainData.journeys_legs_arraycount);
    console.log(`legs array count is ${maxLineCount}`);
    var lineIndex = 0;
    var totalTime = 0;
    while(lineIndex < maxLineCount) {
      if (lineIndex > 1) {
        console.log(` line index ${lineIndex}`);
      }
      var stationCount = plainData[`journeys_0_legs_${lineIndex}_path_stoppoints_arraycount`];
      var stationCountOrdinal = ordinal(parseInt(stationCount));

      directionData[`line_name_${lineIndex+1}`] = plainData[`line_name_${lineIndex+1}`];
      directionData[`line_colour_${lineIndex+1}`] = plainData[`line_colour_${lineIndex+1}`];
      directionData[`line_direction_${lineIndex+1}`] = plainData[`line_direction_${lineIndex+1}`];
      directionData[`change_station_${lineIndex+1}`] = plainData[`change_station_${lineIndex+1}`];
      directionData[`station_count_${lineIndex+1}`] = `${stationCountOrdinal} station`;
      directionData[`station_count_text_${lineIndex+1}`] = `Change train at ${stationCountOrdinal} station`;
      directionData[`journey_time_${lineIndex+1}`] = `${plainData[`journeys_0_legs_${lineIndex}_duration`]} minute${parseInt(plainData[`journeys_0_legs_${lineIndex}_duration`]) > 1 ? 's' : ''}`;
      directionData[`end_station_${lineIndex+1}`] = this.extractEndStation(plainData[`journeys_0_legs_${lineIndex}_instruction_detailed`]);
      var travelTime = (directionData[`journey_time_${lineIndex+1}`] !== null) ? parseInt(directionData[`journey_time_${lineIndex+1}`]) : 0;
      totalTime += travelTime;
      //console.log(directionData);
      lineIndex++;
    }
    directionData[`total_travel_time`] = `${totalTime.toString()} minutes`;
    return directionData;
  }


  populateDirectionDetail(direction) {
    var deferred = Q.defer();
    var promises = [];
    var totalWalkValue = 0;
    var totalEscalatorsValue = 0;
    var totalStairsValue = 0;
    var totalLiftsValue = 0;
    TFLManager.getTFLDirection(direction.icsfrom, direction.icsto).then((tflDirection) => {
      let lineCount = TFLManager.getMaxLineCountFromTFLDirection(tflDirection);

      //for(var i = 0; i < lineCount; i++) {
        var promise = this.populateEntranceData(tflDirection, direction, 0);
        promises.push(promise);
      //}

      Q.all(promises).then((entrancesArray) => {
        entrancesArray.map((entranceItem, entranceIndex) => {
          if (entranceItem !== null) {
            var plainData = entranceItem.get({plain: true});
            totalWalkValue += parseInt(plainData.walk);
            totalStairsValue += parseInt(plainData.stairs);
            totalEscalatorsValue += parseInt(plainData.escalators);
            totalLiftsValue += parseInt(plainData.lifts);
          }
        });

        // Populate the exit data
/*
        this.populateExitData(tflDirection, direction, lineCount).then((exit) => {

          console.log(`total_travel_time params: walk:${totalWalkValue} stairs:${totalStairsValue} escalators:${totalEscalatorsValue} lifts:${totalLiftsValue}`);
          this.populateTotalTravelDetails(direction, totalWalkValue, totalStairsValue, totalEscalatorsValue, totalLiftsValue).then(() => {
            console.log(`finished populating total travel details`);
            deferred.resolve();
          });
        });
        */

        deferred.resolve();
      });
    });

    return deferred.promise;
  }

  populateDirectionDetailExit(direction) {
    var deferred = Q.defer();
    var promises = [];
    var totalWalkValue = 0;
    var totalEscalatorsValue = 0;
    var totalStairsValue = 0;
    var totalLiftsValue = 0;
    TFLManager.getTFLDirection(direction.icsfrom, direction.icsto).then((tflDirection) => {
      let lineCount = TFLManager.getMaxLineCountFromTFLDirection(tflDirection);

        this.populateExitData(tflDirection, direction, lineCount).then((exit) => {
/*
          console.log(`total_travel_time params: walk:${totalWalkValue} stairs:${totalStairsValue} escalators:${totalEscalatorsValue} lifts:${totalLiftsValue}`);
          this.populateTotalTravelDetails(direction, totalWalkValue, totalStairsValue, totalEscalatorsValue, totalLiftsValue).then(() => {
            console.log(`finished populating total travel details`);
            deferred.resolve();
          });
*/
          deferred.resolve();
        });
      });


    return deferred.promise;
  }

  populateExitData(tflDirection, direction, lineIndex) {
    var deferred = Q.defer();
    var tflDirectionPlainData = tflDirection.get({plain: true});
    var directionPlainData = direction.get({plain: true});
    var directionUpdateData = {};

    console.log(`lineIndex passed in : ${lineIndex}`);
    console.log(`line_index to look for : ${lineIndex-1}`);

    const lineName = directionPlainData[`line_name_${lineIndex}`];
    const trainDirection = directionPlainData[`line_direction_${lineIndex}`];
    var stationIcs = tflDirectionPlainData[`journeyvector_to`];//tflDirectionPlainData[`journeyvector_from`];
    if (parseInt(lineIndex) === 1) {
      stationIcs = tflDirectionPlainData[`journeyvector_to`];
      console.log(`using journeyvector_from field ${stationIcs}`);
    }

    var lineNameField = `line_name_${parseInt(lineIndex)+1}`;
    var changeDetailsField = `change_details_${parseInt(lineIndex)+1}`;
    var changeTimeField = `change_time_${parseInt(lineIndex)+1}`;
    var journeyExitField = `journey_exit_${parseInt(lineIndex)}`;

    if (parseInt(lineIndex)+1 > 4) {
      lineNameField = `exit_name_5`;
      changeDetailsField = `exit_details_5`;
      changeTimeField = `exit_time_5`;
    }

    console.log(`populating exit data for direction ${directionPlainData.route}`);

    EntranceManager.getEntrance(lineName, trainDirection, stationIcs).then((exit) => {
      if (exit !== null) {
        console.log(`got exit data for lineIndex: ${parseInt(lineIndex)+1}`);
        var exitPlainData = exit.get({plain: true});
        console.log(exitPlainData);

        directionUpdateData[lineNameField] = 'street';
        directionUpdateData[journeyExitField] = exitPlainData.doors_open;
        directionUpdateData[changeTimeField] = `${exitPlainData.time} minute${parseInt(exitPlainData.time) > 1 ? 's' : ''}`;
        directionUpdateData[changeDetailsField] =
          this.constructEntranceDetailsString(exitPlainData.walk,
            exitPlainData.stairs,
            exitPlainData.escalators,
            exitPlainData.lifts);
        console.log(directionUpdateData);
        direction.update(directionUpdateData).then(() => {
          deferred.resolve(exit);
        });
      }
      else {
        console.log(`No exit data for line: ${parseInt(lineIndex)+1}`);
        deferred.resolve(null);
      }
    });

    return deferred.promise;
  }

  populateEntranceData(tflDirection, direction, lineIndex) {
    var deferred = Q.defer();
    var tflDirectionPlainData = tflDirection.get({plain: true});
    var directionPlainData = direction.get({plain: true});
    var directionUpdateData = {};

    const lineName = directionPlainData[`line_name_1`];
    const trainDirection = tflDirectionPlainData[`line_direction_1`];
    var stationIcs = directionPlainData[`icsfrom`];

    var detailsField = `entrance_details_1`;
    var timeField = `entrance_time_1`;

    console.log(`looking for entrance where lineName: ${lineName} | trainDirection: ${trainDirection} | stationIcs: ${stationIcs}`);

    EntranceManager.getEntrance(lineName, trainDirection, stationIcs).then((entrance) => {


      if (entrance !== null) {
        console.log(`got entrance data for lineIndex: ${lineIndex+1}`);
        var entrancePlainData = entrance.get({plain: true});
        console.log(entrancePlainData);

        directionUpdateData[`line_platform_1`] = entrancePlainData.in_platform;
        directionUpdateData[detailsField] =
          this.constructEntranceDetailsString(entrancePlainData.walk,
            entrancePlainData.stairs,
            entrancePlainData.escalators,
            entrancePlainData.lifts);

        console.log(`${detailsField}: ${directionUpdateData[detailsField]}`);
        var entranceTime = `${entrancePlainData.time} ${entrancePlainData.time > 1 ? 'minutes' : 'minute'}`;
        directionUpdateData[timeField] = entranceTime;
        directionUpdateData[`journey_exit_${lineIndex+1}`] = entrancePlainData.doors_open;
        directionUpdateData[`arrives_from_${lineIndex+1}`] = entrancePlainData.arrives_from;

        direction.update(directionUpdateData).then(() => {
          deferred.resolve(entrance);
        });
      }
      else {
        console.log(`no entrance data for ${directionPlainData.route} lineIndex ${lineIndex+1}`);
        deferred.resolve(null);
      }
    });

    return deferred.promise;
  }

  populateTotalTravelDetails(direction, walk, stairs, escalators, lifts) {
    var deferred = Q.defer();
    var walkString = `${walk}m walk`;
    var stairsString = `${stairs} stair${stairs > 1 ? 's' : ''}`;
    var escalatorsString = `${escalators} escalator${escalators > 1 ? 's' : ''}`
    var liftsString = `LIFT${lifts > 1 ? 's' : ''} available`;

    var finalString = "";
    if (walk > 0) {
      finalString += walkString;
    }
    if (stairs > 0) {
      finalString += (finalString !== '' ? ', ' : '') + stairsString;
    }
    if (escalators > 0) {
      finalString += (finalString !== '' ? ', ' : '') + escalatorsString;
    }
    if (lifts > 0) {
      finalString += (finalString !== '' ? ', ' : '') + '(' + liftsString + ')';
    }

    console.log(`total travel details string is: ${finalString}`);

    direction.update({total_travel_details: finalString}).then(() => {
      deferred.resolve();
    })
    return deferred.promise;
  }

  constructEntranceDetailsString(walkValue, stairsValue, escalatorsValue, liftsValue) {
    var walkString = `${walkValue}m walk`;
    var stairsString = `${stairsValue} stair${stairsValue > 1 ? 's' : ''}`;
    var escalatorsString = `${escalatorsValue} escalator${escalatorsValue > 1 ? 's' : ''}`
    var liftsString = `LIFT${liftsValue > 1 ? 's' : ''} available`;
    var finalString = "";
    if (walkValue > 0) {
      finalString += walkString;
    }

    if (stairsValue > 0) {
      finalString += (finalString !== '' ? ' + ' : '') + stairsString;
    }

    if (escalatorsValue > 0) {
      finalString += (finalString !== '' ? ' + ' : '') + escalatorsString;
    }
    if (liftsString > 0) {
      finalString += (finalString !== '' ? ' + ' : '') + '(' + liftsString + ')';
    }

    console.log(`entrance details string is: ${finalString}`);
    return finalString;
  }

  extractEndStation(instructionDetailedFieldValue) {
    if (instructionDetailedFieldValue !== null && instructionDetailedFieldValue !== undefined) {
      return instructionDetailedFieldValue.split(" towards ")[1];
    }
    else {
      return '';
    }
  }

  processTaxiId(direction) {
    var deferred = Q.defer();
    var plainData = direction.get({plain:true});
    console.log(`processing taxiId for ${plainData.icsfrom} to ${plainData.icsto}`);


    DirectionsManager.getGoogleDistanceData(plainData.icsfrom, plainData.icsto, false).then((result) => {
      console.log(`found taxi id record with id: ${result.record}`);
      var alternativeValue = this.processAlternativeField(direction, result);
      direction.update({taxiid:result.record, alternative: alternativeValue}).then((updateResult) => {
        console.log(`updated ${plainData.icsfrom} to ${plainData.icsto} direction record with taxiId.`);
        deferred.resolve();
      });
    });
    return deferred.promise;
  }

  processAlternativeField(direction, alternativeData) {
    var formattedMiles = Number(alternativeData.miles).toFixed(1);
    var formattedKM = Number(alternativeData.km).toFixed(1);
    var string = `The distance between these two stations is ${formattedMiles} miles (${formattedKM}km) and could take ${alternativeData.time} minutes costing ${alternativeData.cost} in a taxi.`;
    return string;
  }


}

export default new DirectionProcessor();
