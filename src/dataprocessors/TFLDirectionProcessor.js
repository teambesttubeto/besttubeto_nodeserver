import Q from 'q';
import LineManager from '../managers/LineManager';
import ColourManager from '../managers/ColourManager';
import StationManager from '../managers/StationManager';
import DirectionProcessor from './DirectionProcessor';
import Main from '../main';

class TFLDirectionProcessor {

  populateBTTDirectionFromTFLDirection(tflDirection) {
    let directionData = DirectionProcessor.convertTFLDirectionToBTTDirectionData(tflDirection);
    let plainData = tflDirection.get({plain: true});

    Main.models['direction'].findOne({where:{icsto: plainData.journeyvector_to, icsfrom: plainData.journeyvector_from}}).then((direction) => {
      direction.update(directionData).then(() => {
        console.log(`updated direction from ${plainData.journeyvector_from} to ${plainData.journeyvector_to}`);
      });
    });
  }

  convertTFLDataToTFLDirection(rawTFLData) {

    console.log("Beginning conversion of TFL Data to TFL Directions");

    var deferred = Q.defer();
    var finalJourneyDataArray = [];
    var promises = [];
    // Decide if we should quit out of this method as there was an error in the response.
    if (rawTFLData.$type !== undefined) {
      if (rawTFLData.$type.indexOf('DisambiguationResult') > -1) {
        console.error("Cannot use string names for to/from stationid, produces disambiguation results. Use station ICS Codes instead.");
        return null;
      }
    }


    // We always want to work off of the shortest journey available in the response from TFL
    //var journeyData = this.getShortestJourney(rawTFLData.journeys);
    rawTFLData.journeys.map((journeyData, index) => {
      console.log("HERE");
      var journeyDeferred = Q.defer();
      promises.push(journeyDeferred.promise);

      console.log("test");
      var journey = journeyData;
      //var shortestJourneyIndex = journeyData.shortestJourneyIndex;

      console.log(`Got shortest journey with duration ${journey.duration}`);
      var initialOutputData = this.createBlankTFLJourneyDataObject();

      var outputData = {};

      for (var prop in initialOutputData) {
        outputData[prop] = initialOutputData[prop];
      }
      var generalTFLData = {
        btt_route_code: `${this.extractBTTRouteCode(1, rawTFLData.journeyVector.from, rawTFLData.journeyVector.to)}_${index}`,
        journeyvector_from: rawTFLData.journeyVector.from,
        journeyvector_to: rawTFLData.journeyVector.to,
        journeyvector_uri: rawTFLData.journeyVector.uri,
        //journeys: shortestJourneyIndex,
        journeys_legs_arraycount: journey.legs.length
      };
      for (var prop in generalTFLData) {
        outputData[prop] = generalTFLData[prop];
      }
      // Convert the shortest journey into our data format
      this.processJourneyData(journey).then((processedJourneyData) => {
          console.log(`Got converted journey data`);
          journeyDeferred.resolve(Object.assign(outputData, processedJourneyData));

      });
    });
    Q.all(promises).then((promisesArray) => {
      deferred.resolve(promisesArray);
    });
    return deferred.promise;
  }


  getShortestJourney(journeys) {

    var maxTime = 60000;
    var shortestJourney = null;
    var journeyIndex = -1;
    journeys.map((journey, index) => {
      if (parseInt(journey.duration) < maxTime) {
        shortestJourney = journey;
        maxTime = journey.duration;
        journeyIndex = index;
      }
    });
    return {shortestJourneyIndex: journeyIndex, shortestJourney};
  }

  extractBTTJourneyName(from, to) {
    return
  }

  extractBTTRouteCode(languageIndex, from, to) {
    var fromTrimmed = from.substring(from.length-4, from.length);
    var toTrimmed = to.substring(to.length-4, to.length);
    return `${languageIndex}${toTrimmed}${fromTrimmed}`;
  }

  createBlankTFLJourneyDataObject() {
    return {
      journeyvector_uri: null,
      journeys: null,
      journeys_legs_arraycount: null,
      journeys_0_legs_0_path_stoppoints_arraycount: null,
      journeys_0_legs_0_duration: null,
      journeys_0_legs_0_instruction_summary: null,
      line_name_1: null,
      journeys_0_legs_0_arrivalpoint_icscode: null,
      change_station_1: null,
      journeys_0_legs_0_instruction_detailed: null,
      journeys_0_legs_0_stoppoint0id: null,
      line_direction_1: null,
      line_colour_1: null,
      journeys_0_legs_1_path_stoppoints_arraycount: null,
      journeys_0_legs_1_duration: null,
      journeys_0_legs_1_instruction_summary: null,
      line_name_2: null,
      journeys_0_legs_1_arrivalpoint_icscode: null,
      change_station_2: null,
      journeys_0_legs_1_instruction_detailed: null,
      journeys_0_legs_1_stoppoint0id: null,
      line_direction_2: null,
      line_colour_2: null,
      journeys_0_legs_2_path_stoppoints_arraycount: null,
      journeys_0_legs_2_duration: null,
      journeys_0_legs_2_instruction_summary: null,
      line_name_3: null,
      journeys_0_legs_2_arrivalpoint_icscode: null,
      change_station_3: null,
      journeys_0_legs_2_instruction_detailed: null,
      journeys_0_legs_2_stoppoint0id: null,
      line_direction_3: null,
      line_colour_3: null,
      journeys_0_legs_3_path_stoppoints_arraycount: null,
      journeys_0_legs_3_duration: null,
      journeys_0_legs_3_instruction_summary: null,
      line_name_4: null,
      journeys_0_legs_3_arrivalpoint_icscode: null,
      change_station_4: null,
      journeys_0_legs_3_instruction_detailed: null,
      journeys_0_legs_3_stoppoint0id: null,
      line_direction_4: null,
      line_colour_4: null
    };
  }

  populateTFLLineData(tflRowSetBatches) {
    var deferred = Q.defer();

    console.log("Populating TFL Line data...");
    var chain = tflRowSetBatches.reduce((previous, batch) => {
      return previous.then((previousValue) => {
        console.log(""); // Blank line to seperate logs
        return this.populateTFLRowLineDataBatch(batch);
      });
    }, Q.resolve()).then(() => {
      deferred.resolve();
    });

    return deferred.promise;
  }

  populateTFLRowLineDataBatch(tflRowSetBatch) {
    var deferred = Q.defer();
    var promises = [];
    console.log(`processing batch, size: ${tflRowSetBatch.length}`);
    tflRowSetBatch.map((item, index) => {

      var deferred = Q.defer();

      this.resetTFLRowLineData(item).then(() => {
        this.processTFLRowLineData(item).then(() => {
          deferred.resolve();
        });
      });

      promises.push(deferred.promise);

    });

    Q.all(promises).then(() => {
      console.log("");
      console.log("******** RESOLVED THIS *******");
      console.log("");
      deferred.resolve();
    });

    return deferred.promise;
  }

  processTFLRowLineData(tflDirection) {
    var deferred = Q.defer();



    var promises = [];

    var plainData = tflDirection.get({plain: true});
    console.log(`processing tfl line data for record: ${plainData.btt_route_code}`);
    for(var i = 0; i < 3; i++) {

      promises.push(this.processTFLRowLineDataIndex(tflDirection, i));
    }

    Q.all(promises).then(() => {
      deferred.resolve();
    });

    return deferred.promise;
  }

  resetTFLRowLineData(tflDirection) {
    var deferred = Q.defer();
    var plainData = tflDirection.get({plain: true});
    console.log(`resetting line and colour data for btt_route_code: ${plainData.btt_route_code}`);

    var updateData = {
      line_direction_1 : null,
      line_direction_2 : null,
      line_direction_3 : null,
      line_direction_4 : null,
      line_colour_1 : null,
      line_colour_2 : null,
      line_colour_3 : null,
      line_colour_4 : null,
    };

    tflDirection.update(updateData).then(() => {
      console.log(`successfully reset line and colour data for btt_route_code: ${plainData.btt_route_code}.`);
      deferred.resolve();
    });
    return deferred.promise;
  }

  processTFLRowLineDataIndex(tflDirection, lineIndex) {
    var plainData = tflDirection.get({plain: true});
    var deferred = Q.defer();
    var fromICSCode = null;
    var nextNaptanId = null;
    var processedLineData = {};
    var lineName = plainData[`line_name_${lineIndex+1}`] !== null ? plainData[`line_name_${lineIndex+1}`].split(" ")[0] : '';

    if (lineIndex === 0) {
      // For first line, get journeyvector_from (ICS code) and journeys_0_legs_0_stoppoint0id (Naptan ID) from lines table.
      fromICSCode = plainData[`journeyvector_from`];
      nextNaptanId = plainData[`journeys_0_legs_0_stoppoint0id`];
    }
    else {
      // For all subsequent lines, get journeys_0_legs_${LINECOUNT-1}_arrivalpoint_icscode (ICS code) and journeys_0_legs_${LINECOUNT}_stoppoint0id (Naptan ID) from lines table.
      fromICSCode = plainData[`journeys_0_legs_${lineIndex-1}_arrivalpoint_icscode`];
      nextNaptanId = plainData[`journeys_0_legs_${lineIndex}_stoppoint0id`];
    }

    LineManager.getLine(fromICSCode, nextNaptanId, lineName).then((lineData) => {

      if (lineData !== null) {
        console.log(`Setting line data for ${plainData.btt_route_code}, line index: ${parseInt(lineIndex)+1}`);

        processedLineData[`line_direction_${lineIndex+1}`] = lineData.direction;
        processedLineData[`line_colour_${lineIndex+1}`] = lineData.line_colour;

        tflDirection.update(processedLineData).then(() => {
          console.log(`updated TFL direction row ${plainData.btt_route_code}`);
          deferred.resolve();
        });
      }
      else {
        console.log(`No line data found for ${plainData.btt_route_code}`);
        deferred.resolve();
      }
    });
    return deferred.promise;
  }

  processTFLLineData(legsArray, leg, theLineCount) {
    var deferred = Q.defer();
    var processedLineData = {};
    var fromICSCode = null;
    var nextNaptanId = null;

    if (theLineCount === 0) {
      // For first line, get journeyvector_from (ICS code) and journeys_0_legs_0_stoppoint0id (Naptan ID) from lines table.
      fromICSCode = leg.departurePoint.icsCode;
      nextNaptanId = leg.path.stopPoints[0].id;
    }
    else {
      // For all subsequent lines, get journeys_0_legs_${LINECOUNT-1}_arrivalpoint_icscode (ICS code) and journeys_0_legs_${LINECOUNT}_stoppoint0id (Naptan ID) from lines table.
      //fromICSCode = legsArray[theLineCount-1].arrivalPoint.icsCode
      fromICSCode = leg.arrivalPoint.icsCode;
      nextNaptanId = leg.path.stopPoints[0].id;
    }
    LineManager.getLine(fromICSCode, nextNaptanId).then((lineData) => {
      if (lineData !== null) {
        console.log(`Setting line data for ${leg.routeOptions[0].lineIdentifier.id}, line_direction_${theLineCount+1}=${lineData.direction}, line_color_${theLineCount+1}=${lineData.line_colour}`);
        processedLineData[`line_direction_${theLineCount+1}`] = lineData.direction;
        processedLineData[`line_colour_${theLineCount+1}`] = lineData.line_colour;
      }
      else {
        console.log(`No line data found for ${leg.routeOptions[0].lineIdentifier.id}`);
      }
      deferred.resolve(processedLineData);
    });
    return deferred.promise;
  }

  processJourneyData(journey) {
    var deferred = Q.defer();
    var journeyData = {};
    var promises = [];

    var relative_line_count = 0;

    journey.legs.map((leg, line_count) => {

      // Only process the leg if it's a tube/dlr mode of transport
      //if (leg.mode.id === 'tube' || leg.mode.id === 'dlr') {
      if (1===1){

        console.log(`processing journey leg index ${relative_line_count}`);
        journeyData[`journeys_0_legs_${relative_line_count}_path_stoppoints_arraycount`] = leg.path.stopPoints.length;
        journeyData[`journeys_0_legs_${relative_line_count}_duration`] = leg.duration;
        journeyData[`journeys_0_legs_${relative_line_count}_instruction_summary`] = leg.instruction.summary;
        journeyData[`journeys_0_legs_${relative_line_count}_instruction_detailed`] = leg.instruction.detailed;
        journeyData[`line_name_${relative_line_count+1}`] = this.extractTFLLineName(leg.instruction.summary);
        journeyData[`journeys_0_legs_${relative_line_count}_arrivalpoint_icscode`] = leg.arrivalPoint.icsCode;
        if (leg.path.stopPoints.length > 0) {
          journeyData[`journeys_0_legs_${relative_line_count}_stoppoint0id`] = leg.path.stopPoints[0].id;
        }

        // Asynchronously get the Station at the arrival point
        var stationDeferred = Q.defer();
        const theLineCount = relative_line_count;

        StationManager.getStationByICSCode(leg.arrivalPoint.icsCode, theLineCount).then((stationData) => {
          if (stationData !== null) {
            console.log(`Got station ${stationData.stationname} with ICS code ${leg.arrivalPoint.icsCode}`);
            journeyData[`change_station_${theLineCount+1}`] = stationData.stationname;
          }
          else {
            console.log(`no station for ICS code ${leg.arrivalPoint.icsCode}`);
          }
          stationDeferred.resolve();
        })
        promises.push(stationDeferred.promise);

        // Asynchronously get the line associated with this leg
        /*
        if (leg.routeOptions[0].lineIdentifier !== undefined) {
          var tflLineId = leg.routeOptions[0].lineIdentifier.id;

          if (tflLineId !== null) {
            var linePromise = this.processTFLLineData(journey.legs, leg, theLineCount);
            promises.push(linePromise);
          }
        }
        */
        relative_line_count++;
      }
    });

    Q.all(promises).then((promiseArray) => {
        console.log(`Finished creating TFL journey data`);
        promiseArray.map((obj, index) => {
          console.log(obj);
          journeyData = Object.assign(journeyData, obj);
        });

        deferred.resolve(journeyData);
    });

    return deferred.promise;
  }


  extractTFLLineName(summaryString) {
    return summaryString.split(" to ")[0];
  }
}


export default new TFLDirectionProcessor();
